#pragma once

#include "Scene.h"
#include "simpledrawing.h"

struct rect_t
{
  int x1, y1, x2, y2;
};

#define WORK_PIECE_SIZE 50

/// Class for ThreadWork handling.
class ThreadWork 
{
private:
  HANDLE threadHandle;

  bool perform, terminated, performed;

  static DWORD ThreadProc(LPVOID lpdwThreadParam);
  static DWORD WorkerProc(LPVOID lpdwThreadParam);

  void InitWork();
  void Run();

  Scene*         scene;
  SimpleDrawing* simpleDraw;
  std::vector<HANDLE> workers;
  HANDLE         mutexFetchWork;
  std::vector<rect_t> rectWork;
  int                 indexWork;

  DWORD  renderTimeMs;

  /// Fetch work for one thread.
  bool FetchWork(rect_t& r);
  /// Renders specified rect.
  void ProcessRaytracer(const rect_t& r);

public:
  /// ThreadWork constructor.
  ThreadWork(Scene* sc, SimpleDrawing* sd);
  ~ThreadWork(void);
  
  void Perform() { perform = true; }

  /// Get info about process.
  void GetProcessInfo(char* strInfo);
  /// Get info about process.
  void GetFullProcessInfo(char* strInfo);

  bool IsWorking() { return perform; }
  bool ImageWasRendered() { return performed; }

  static int WorkerCount() 
  { 
    int numThreads;
    SYSTEM_INFO sysinfo;    
    GetSystemInfo( &sysinfo );
#ifdef _DEBUG
    numThreads = 1;
#else
    numThreads = sysinfo.dwNumberOfProcessors;
#endif
    //numThreads = 1; // uncomment to disable paralel computing :)
    return numThreads;
  }
};