#pragma once
#include "simplemath.h"

/// Trida pro rasterizaci grafiky bez OpenGL/DX
class SimpleDrawing
{
private:
  /// handle okna, do ktereho kreslime
  HWND   hwnd;
  /// device context okna
  HDC    windowDC;

  HANDLE semaphoreResizing;
  int semaphoreMax;

  /// sirka a vyska bufferu
  int width, height;
  /// color buffer
  FLOAT_TYPE*      colorBuffer;  
  BYTE*      colorBufferBytes;  
  /// handle na bitmapu obsahujici color buffer
  HBITMAP    colorBufferHandle;
  /// informace o bitmape
  BITMAPINFO colorBufferBI;
  /// device context color bufferu
  HDC        colorBufferDC;

  bool restoreBuffers;

  // -----------------------
  
  /// je objekt pouzitelny?
  bool state_ok;
  /// inicializace bitmap (color buffer, depth buffer)
  void InitBuffers();
  /// inicializace bitmap (color buffer, depth buffer)
  void DeinitBuffers();
  
  // -----------------------

  inline void recomputeToPixelCoords(const float x, const float y, int& ix, int& iy);
 
protected:  

public:  
  /// konstruktor
  SimpleDrawing(HWND pHwnd, HDC pWindowDC);
  /// destruktor
  ~SimpleDrawing(void);  

  /// zavolat pri zmene velikosti okna
  void OnWindowResize();
  /// vymaze obsah bufferu (nahradi ho danou barvou)
  void ClearColor(FLOAT_TYPE r, FLOAT_TYPE g, FLOAT_TYPE b, FLOAT_TYPE a = 1.0);
  /// Vykresli pixel dane barvy na pozici x, y
  void PutPixel(const int x, const int y, const FLOAT_TYPE r, const FLOAT_TYPE g, const FLOAT_TYPE b, const FLOAT_TYPE a = 1.0);
  /// vykresli buffer do okna
  void RenderBuffer(HDC destHDC);

  int GetBufferWidth() { return width; }
  int GetBufferHeight() { return height; }
  HWND GetHWND() { return hwnd; }

  bool SaveToPNG(const char* filename, char* errormsg);
};