#pragma once

#include "simplemath.h"

class ThreadWork;
class Scene;

/// Class loading scene from text file.
class SceneLoader
{
private:
  Scene* scene;
  ThreadWork* threadWork;

  int                   anonymousObjectsIdCounter;

  int                   tokId;
  int                   tokType;
  std::vector<double>   tokValues;
  std::map<int, int>    typeParamCount;

  enum OPMODE {opModeUnion, opModeIntersect, opModeMinus};

  bool Command_Neorientovana_Rovina();
  bool Command_Poloprostor();
  bool Command_Elipsoid(sm::vec3 stred, sm::vec3 osyVelikost);
  bool Command_Kvadr();
  bool Command_Kvadrika();
  bool Command_Obdelnik();
  bool Command_Render();
  bool Command_Bodove_Svetlo();
  bool Command_Max_Hloubka();
  bool Command_Op(OPMODE opMode);
  bool Command_Barva(int typeIndex);
  bool Command_Index_Lomu();
  bool Command_Rozmazani();
  bool Command_Kamera();
  bool Command_MonoKamera();
  bool Command_Transformace(const sm::mat4& transformace);
  bool Command_Sachovnice_Tex();

  void InitTables();
  bool ProcessCommand();

public:
  char err[256];
  SceneLoader(Scene* scene, ThreadWork* threadWork);
  ~SceneLoader(void);

  bool Load(const char* filename);
};

