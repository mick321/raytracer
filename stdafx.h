// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

//#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <io.h>
#include <fcntl.h>

#include <string>
#include <vector>
#include <set>
#include <map>
#include <algorithm>

// PNG support
#pragma warning(disable:4355)
#include "png++/png.hpp"
#pragma warning(default:4355)