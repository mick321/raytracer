#include "StdAfx.h"
#include "simpledrawing.h"
#include "pngsave.h"
#include "ThreadWork.h"

SimpleDrawing::SimpleDrawing(HWND pHwnd, HDC pWindowDC)
{
  hwnd = pHwnd;
  windowDC = pWindowDC;
  state_ok = false;
  restoreBuffers = false;

  colorBuffer = NULL;
  colorBufferHandle = NULL;
  colorBufferDC = NULL;
  colorBufferBytes = NULL;

  semaphoreMax = ThreadWork::WorkerCount();
  semaphoreResizing = CreateSemaphore(NULL, semaphoreMax, semaphoreMax, NULL);

  OnWindowResize();
}


SimpleDrawing::~SimpleDrawing(void)
{
  CloseHandle(semaphoreResizing);
}

/// provede se pri zmene velikosti okna
void SimpleDrawing::OnWindowResize()
{
  for (int i = 0; i < semaphoreMax; i++)
    WaitForSingleObject(semaphoreResizing, INFINITE);

  WINDOWPLACEMENT wndPlacement;
  wndPlacement.length = sizeof(WINDOWPLACEMENT);
  GetWindowPlacement(hwnd, &wndPlacement);
  if (wndPlacement.showCmd == SW_MINIMIZE || wndPlacement.showCmd == SW_SHOWMINIMIZED || wndPlacement.showCmd == SW_HIDE)
  {
    ReleaseSemaphore(semaphoreResizing, semaphoreMax, NULL);
    return;
  }

  int oldwidth = 0;
  FLOAT_TYPE* oldcolors = NULL;
  if (colorBuffer)
  {
    oldwidth = width;
    oldcolors = new FLOAT_TYPE[4 * width * height];
    memcpy(oldcolors, colorBuffer, 4 * width * height * sizeof(FLOAT_TYPE));
  }
  DeinitBuffers();

  RECT client_rect;
  ::GetClientRect(hwnd, &client_rect);
  width = max(1, client_rect.right - client_rect.left);
  height = max(1, client_rect.bottom - client_rect.top);

  InitBuffers();

  if (oldcolors)
  {
    for (int j = 0; j < height; j++)
      for (int i = 0; i < width; i++)
      {
        colorBuffer[(j * width + i)*4] = oldcolors[(j * oldwidth + i)*4];
        colorBuffer[(j * width + i)*4+1] = oldcolors[(j * oldwidth + i)*4+1];
        colorBuffer[(j * width + i)*4+2] = oldcolors[(j * oldwidth + i)*4+2];
        colorBuffer[(j * width + i)*4+3] = oldcolors[(j * oldwidth + i)*4+3];
      }

    delete[] oldcolors;
  }

  ReleaseSemaphore(semaphoreResizing, semaphoreMax, NULL);
}

/// inicializace bitmap (color buffer, depth buffer)
void SimpleDrawing::InitBuffers()
{
  colorBuffer = new FLOAT_TYPE[4 * width * height]; // RGBA * w * h
  colorBufferBytes = new BYTE[4 * width * height]; // RGBA * w * h
  for (int i = 0; i < width * height; i++)
  {
    colorBuffer[i * 4 + 0] = 0;
    colorBuffer[i * 4 + 1] = 0;
    colorBuffer[i * 4 + 2] = 0;
    colorBuffer[i * 4 + 3] = 1;
  }  
}

/// inicializace bitmap (color buffer, depth buffer)
void SimpleDrawing::DeinitBuffers()
{
  if (colorBufferDC)
  {
    ::DeleteDC(colorBufferDC);
    colorBufferDC = NULL;
  }

  if (colorBufferHandle)
  {    
    ::DeleteObject(colorBufferHandle);
    colorBufferHandle = NULL;
  }

  if (colorBuffer)
  {
    delete[] colorBuffer;    
    colorBuffer = NULL;
  }

  if (colorBufferBytes)
  {
    delete[] colorBufferBytes;
    colorBufferBytes = NULL;
  }
}

/// vymaze obsah bufferu (nahradi ho danou barvou)
void SimpleDrawing::ClearColor(FLOAT_TYPE r, FLOAT_TYPE g, FLOAT_TYPE b, FLOAT_TYPE a)
{
  for (int i = 0; i < semaphoreMax; i++)
    WaitForSingleObject(semaphoreResizing, INFINITE);
  for (int i = 0; i < width * height; i++)
  {
    colorBuffer[i * 4 + 0] = b;
    colorBuffer[i * 4 + 1] = g;
    colorBuffer[i * 4 + 2] = r;
    colorBuffer[i * 4 + 3] = a;
  }  
  ReleaseSemaphore(semaphoreResizing, semaphoreMax, NULL);
}

/// Vykresli pixel dane barvy na pozici x, y
void SimpleDrawing::PutPixel(const int x, const int y, const FLOAT_TYPE r, const FLOAT_TYPE g, const FLOAT_TYPE b, const FLOAT_TYPE a)
{ 
  WaitForSingleObject(semaphoreResizing, INFINITE);
  if (x < 0 || x >= width || y < 0 || y >= height)
    return;
  const int yy = height - y - 1;
  colorBuffer[(yy * width + x) * 4 + 0] = b;
  colorBuffer[(yy * width + x) * 4 + 1] = g;
  colorBuffer[(yy * width + x) * 4 + 2] = r;
  colorBuffer[(yy * width + x) * 4 + 3] = a;
  ReleaseSemaphore(semaphoreResizing, 1, NULL);
}


/// vykresli buffer do okna
void SimpleDrawing::RenderBuffer(HDC destHDC)
{
  WINDOWPLACEMENT wndPlacement;
  wndPlacement.length = sizeof(WINDOWPLACEMENT);
  GetWindowPlacement(hwnd, &wndPlacement);
  if (wndPlacement.showCmd == SW_MINIMIZE || wndPlacement.showCmd == SW_SHOWMINIMIZED || wndPlacement.showCmd == SW_HIDE)
  {
    restoreBuffers = true;
    return;
  }
  if (restoreBuffers)
  {
    OnWindowResize();
    restoreBuffers = false;
  }

  if (!colorBufferHandle)
    colorBufferHandle = CreateCompatibleBitmap(destHDC, width, height);
  if (!colorBufferDC)
  {
    colorBufferDC =  CreateCompatibleDC(destHDC);      
    ZeroMemory(&colorBufferBI, sizeof(BITMAPINFO));
    colorBufferBI.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    colorBufferBI.bmiHeader.biWidth = width;
    colorBufferBI.bmiHeader.biHeight = height;
    colorBufferBI.bmiHeader.biPlanes = 1;
    colorBufferBI.bmiHeader.biBitCount = 32;
    colorBufferBI.bmiHeader.biCompression = BI_RGB;
  }

  FLOAT_TYPE maxvalue = 0.0000001;  
  for (int i = 0; i < height * width; i++)  
  {
    if (colorBuffer[i*4+3] > 0.0)
      maxvalue = max(colorBuffer[i*4], max(colorBuffer[i*4+1], max(colorBuffer[i*4+2], maxvalue)));
  }
  
  FLOAT_TYPE invmaxcoef = 254.5 / maxvalue;
  /*
  FLOAT_TYPE invmaxcoef = 128.0 / maxvalue;
  */
  for (int i = 0; i < height * width; i++)
  {
    if (colorBuffer[i*4+3] > 0.0)
    {
      colorBufferBytes[i*4] = (BYTE)(invmaxcoef * colorBuffer[i*4]);
      colorBufferBytes[i*4+1] = (BYTE)(invmaxcoef * colorBuffer[i*4+1]);
      colorBufferBytes[i*4+2] = (BYTE)(invmaxcoef * colorBuffer[i*4+2]);
      /*
      colorBufferBytes[i*4] = (BYTE)(invmaxcoef * colorBuffer[i*4] / (colorBuffer[i*4] + 1));
      colorBufferBytes[i*4+1] = (BYTE)(invmaxcoef * colorBuffer[i*4+1] / (colorBuffer[i*4+1] + 1));
      colorBufferBytes[i*4+2] = (BYTE)(invmaxcoef * colorBuffer[i*4+2] / (colorBuffer[i*4+2] + 1));
      */
      colorBufferBytes[i*4+3] = 255;
    }
    else
    {
      colorBufferBytes[i*4] = (BYTE)(255 * colorBuffer[i*4]);
      colorBufferBytes[i*4+1] = (BYTE)(255 * colorBuffer[i*4+1]);
      colorBufferBytes[i*4+2] = (BYTE)(255 * colorBuffer[i*4+2]);
      colorBufferBytes[i*4+3] = 255;
    }
  }

  SetDIBitsToDevice(destHDC, 0, 0, width, height, 0, 0, 0, height, colorBufferBytes, &colorBufferBI, 0);
}


bool SimpleDrawing::SaveToPNG(const char* filename, char* errormsg)
{
  BYTE* mem = (BYTE*)malloc(4 * sizeof(BYTE) * width * height);
  for (int j = 0; j < width * height; j++)
  {
    mem[j*4] = colorBufferBytes[j*4+2];
    mem[j*4+1] = colorBufferBytes[j*4+1];
    mem[j*4+2] = colorBufferBytes[j*4+0];
    mem[j*4+3] = colorBufferBytes[j*4+3];
  }

  bool rtn = SaveBitmapToPNG(filename, width, height, mem, errormsg);
  free(mem);
  return rtn;
}