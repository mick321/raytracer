#include "stdafx.h"
#include "Scene.h"

void IntersectInterval::MergeAandB(IntersectInterval& A, IntersectInterval& B, const sm::ray& ray, node::Material* newMaterial)
{
  int ia = 0, ib = 0;
  int acount = A.intersections.size();
  int bcount = B.intersections.size();
  bool insideA, insideB;

  insideA = A.startingInside;
  insideB = B.startingInside;
  startingInside = insideA && insideB;
  startingEta = startingInside ? newMaterial->eta : 1;

  while (ia < acount && ib < bcount)
  {
    if (A.intersections[ia].t <= B.intersections[ib].t)
    {
      IntersectInfo& ii = A.intersections[ia];
      if (insideB)
      {
        ii.material = newMaterial;
        ii.etaIn = newMaterial->eta;
        ii.etaOut = 1;
        AddIntersection(ii);
      }
      if (ii.switchInside)
        insideA = !insideA;
      ia++;
    }
    else
    {
      IntersectInfo& ii = B.intersections[ib];
      if (insideA)
      {
        ii.material = newMaterial;
        ii.etaIn = newMaterial->eta;
        ii.etaOut = 1;
        AddIntersection(ii);
      }
      if (ii.switchInside)
        insideB = !insideB;
      ib++;
    }
  }

  while (ia < acount)
  {
    IntersectInfo& ii = A.intersections[ia];
    if (insideB)
    {
      ii.material = newMaterial;
      ii.etaIn = newMaterial->eta;
      ii.etaOut = 1;
      AddIntersection(ii);
    }
    if (ii.switchInside)
      insideA = !insideA;
    ia++;
  }
  while (ib < bcount)
  {
    IntersectInfo& ii = B.intersections[ib];
    if (insideA)
    {
      ii.material = newMaterial;
      ii.etaIn = newMaterial->eta;
      ii.etaOut = 1;
      AddIntersection(ii);
    }
    if (ii.switchInside)
      insideB = !insideB;
    ib++;
  }
}

/// Merge two intersectintervals into this using (A or B implemented as (A or (B - A))).
void IntersectInterval::MergeAorB(IntersectInterval& A, IntersectInterval& B, const sm::ray& ray)
{
  int ia = 0, ib = 0;
  int acount = A.intersections.size();
  int bcount = B.intersections.size();
  bool insideA, insideB;

  insideA = A.startingInside;
  insideB = B.startingInside;
  startingInside = insideA || insideB;
  startingEta = insideA ? A.startingEta : (insideB ? B.startingEta : 1);
  FLOAT_TYPE etaOutOfB = insideA ? A.startingEta : 1;
  FLOAT_TYPE etaOutOfA = insideB ? B.startingEta : 1;

  while (ia < acount && ib < bcount)
  {
    if (A.intersections[ia].t <= B.intersections[ib].t)
    {
      IntersectInfo& ii = A.intersections[ia];
      IntersectInfo add = ii; 
      add.switchInside = add.switchInside && !insideB;
      add.etaOut = insideB ? etaOutOfA : add.etaOut;
      if (!insideB || add.etaOut != add.etaIn || add.material->transparent.w == 0)
        AddIntersection(add);

      if (ii.switchInside)
        insideA = !insideA;

      etaOutOfB = insideA ? ii.etaIn : ii.etaOut;

      ia++;
    }
    else
    {
      IntersectInfo& ii = B.intersections[ib];
      if (!insideA)
      {
        IntersectInfo& added = AddIntersection(ii);
        added.switchInside = added.switchInside && !insideA;
        added.etaOut = insideA ? etaOutOfB : added.etaOut;
      }

      if (ii.switchInside)
        insideB = !insideB;

      etaOutOfA = insideA ? ii.etaIn : ii.etaOut;

      ib++;
    }
  }

  while (ia < acount)
  {
    IntersectInfo& ii = A.intersections[ia];
    IntersectInfo& added = AddIntersection(ii);
    added.switchInside = added.switchInside && !insideB;
    added.etaOut = insideB ? etaOutOfA : added.etaOut;

    if (ii.switchInside)
      insideA = !insideA;

    etaOutOfB = insideA ? ii.etaIn : ii.etaOut;

    ia++;
  }
  while (ib < bcount)
  {
    IntersectInfo& ii = B.intersections[ib];
    if (!insideA)
    {
      IntersectInfo& added = AddIntersection(ii);
      added.switchInside = added.switchInside && !insideA;
      added.etaOut = insideA ? etaOutOfB : added.etaOut;
    }

    if (ii.switchInside)
      insideB = !insideB;

    etaOutOfA = insideA ? ii.etaIn : ii.etaOut;

    ib++;
  }
}

/// Merge two intersectintervals into this using (A minus B).
void IntersectInterval::MergeAminusB(IntersectInterval& A, IntersectInterval& B, const sm::ray& ray)
{
  int ia = 0, ib = 0;
  int acount = A.intersections.size();
  int bcount = B.intersections.size();
  bool insideA, insideB;

  insideA = A.startingInside;
  insideB = B.startingInside;
  startingInside = insideA && !insideB;
  FLOAT_TYPE etaOutOfB = insideA ? A.startingEta : 1;

  while (ia < acount && ib < bcount)
  {
    if (A.intersections[ia].t <= B.intersections[ib].t)
    {
      IntersectInfo& ii = A.intersections[ia];
      if (ii.switchInside)
        insideA = !insideA;

      etaOutOfB = insideA ? ii.etaIn : ii.etaOut;
      
      if (!insideB)
        AddIntersection(ii);

      ia++;
    }
    else
    {
      IntersectInfo& ii = B.intersections[ib];
      if (ii.switchInside)
        insideB = !insideB;
      
      if (insideA)
      {
        IntersectInfo& added = AddIntersection(ii);
        added.normal = -added.normal;
        added.etaOut = 1;
        added.etaIn = etaOutOfB;
      }
      
      ib++;
    }
  }

  while (ia < acount)
  {
    IntersectInfo& ii = A.intersections[ia];
    if (ii.switchInside)
      insideA = !insideA;

    etaOutOfB = insideA ? ii.etaIn : ii.etaOut;
      
    if (!insideB)
      AddIntersection(ii);

    ia++;
  }
  while (ib < bcount)
  {
    IntersectInfo& ii = B.intersections[ib];
    if (ii.switchInside)
      insideB = !insideB;
      
    if (insideA)
    {
      IntersectInfo& added = AddIntersection(ii);
      added.normal = -added.normal;
      added.etaOut = 1;
      added.etaIn = etaOutOfB;
    }
      
    ib++;
  }
}

namespace node
{
  // -----------------------------------------------------

  Camera::Camera()
  {
    Setup(sm::vec3(0, 3, 2), sm::vec3(0, -1, -1), sm::vec3(0, 1, 0), (FLOAT_TYPE)75);
    /*
    source.set(0, 0, 6);
    topLeft.set(-3, 3, 2);
    topRight.set(3, 3, 2);
    bottomLeft.set(-3, -3, 2);
    */
  }
  
  sm::ray Camera::GetRay(FLOAT_TYPE u, FLOAT_TYPE v)
  {
    sm::ray rtn;
    sm::vec3 deltaX = (topRight - topLeft);
    sm::vec3 deltaY = (bottomLeft - topLeft);
    rtn.pos = topLeft + deltaX * u + deltaY * v;
    rtn.dir = rtn.pos - source;
    return rtn;
  }

  void Camera::Setup(const sm::vec3& position, const sm::vec3& direction, const sm::vec3& upDirection, const FLOAT_TYPE fov)
  {
    const FLOAT_TYPE fovDiv2Rad = fov * PI180 * 0.5;
    sm::vec4 topLeft4(-tan(fovDiv2Rad), tan(fovDiv2Rad), 0, 1);
    sm::vec4 topRight4(-topLeft4.x, topLeft4.y, topLeft4.z, 1);
    sm::vec4 bottomLeft4(topLeft4.x, -topLeft4.y, topLeft4.z, 1);
    sm::vec4 source4(0, 0, -1, 1);
    sm::mat4 mat;

    sm::vec3 rightDirection = sm::cross(direction, upDirection);
    sm::vec3 upDirectionOrtho = sm::cross(rightDirection, direction);
    rightDirection = sm::normalize(rightDirection) * sm::length(direction);
    upDirectionOrtho = sm::normalize(upDirectionOrtho) * sm::length(direction);

    mat.m11 = rightDirection.x; mat.m12 = rightDirection.y; mat.m13 = rightDirection.z; mat.m14 = 0;
    mat.m21 = upDirectionOrtho.x; mat.m22 = upDirectionOrtho.y; mat.m23 = upDirectionOrtho.z; mat.m24 = 0;
    mat.m31 = direction.x; mat.m32 = direction.y; mat.m33 = direction.z; mat.m34 = 0;
    mat.m41 = 0; mat.m42 = 0; mat.m43 = 0; mat.m44 = 1;
    mat = sm::mat4::translation(position) * mat.transpose();

    topLeft = (mat * topLeft4).xyz();
    topRight = (mat * topRight4).xyz();
    bottomLeft = (mat * bottomLeft4).xyz();
    source = (mat * source4).xyz();
  }

  // -----------------------------------------------------

  void Solid::Prepare()
  {
    SceneNode::Prepare();
    material.opacity = sm::vec4(1, 1, 1, 1) - material.transparent;
  }

  void Solid::OnCreate()
  {
    materialchanged = false;
  }

  // -----------------------------------------------------
  /// Solve intersection ray vs. this object.
  bool Quadric::Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats)
  {
    localStats.intersectionCount.PlusOne();
    localStats.intersectionCount.PlusOne();

    IntersectInfo interInfo;
    interInfo.objId = id;
    interInfo.etaIn = material.eta;
    interInfo.etaOut = 1;

    sm::vec4 x_a = sm::vec4(r.pos, 1);
    sm::vec4 s = sm::vec4(r.dir, 0);
    sm::vec4 Q_DOT_x_a = quadric_m * x_a;
    sm::vec4 Q_DOT_s = quadric_m * s;

    FLOAT_TYPE a, b, c;
    a = sm::dot(s, Q_DOT_s);
    b = sm::dot(s, Q_DOT_x_a) + sm::dot(x_a, Q_DOT_s);
    c = sm::dot(x_a, Q_DOT_x_a);

    FLOAT_TYPE t1 = -1, t2 = -1;
    intersectInterval.startingInside = c <= 0;
    if (sm::solveQuadraticEquation(a, b, c, t1, t2))
    {
      if (t1 >= 0)
      {
        interInfo.pos = r.pos + r.dir * t1;
        sm::vec4 gradient = sm::quadricGradient(quadric_m, sm::vec4(interInfo.pos, 1));
        //gradient = gradient * ((FLOAT_TYPE)1 / abs(gradient.w));
        interInfo.normal = normalize(gradient.xyz());
        interInfo.material = &material;
        interInfo.t = t1;
        intersectInterval.AddIntersection(interInfo);
      }
      if (t2 >= 0)
      {
        interInfo.pos = r.pos + r.dir * t2;
        sm::vec4 gradient = sm::quadricGradient(quadric_m, sm::vec4(interInfo.pos, 1));
        //gradient = gradient * ((FLOAT_TYPE)1 / abs(gradient.w));
        interInfo.normal = normalize(gradient.xyz());
        interInfo.material = &material;
        interInfo.t = t2;
        intersectInterval.AddIntersection(interInfo);
      }
    }

    return intersectInterval.intersections.size() > 0;
  }

  /// Prepare coefs with applied matrix.
  void Quadric::Prepare()
  {
    Solid::Prepare();
    quadric_m = inverseMatrix.transpose() * quadric * inverseMatrix;
    quadric_m_t = quadric_m.transpose();
  }

  /// Setup quadric coefs to make a sphere.
  void Quadric::InitAsSphere(const sm::vec3& center, const FLOAT_TYPE radius)
  {
    InitAsEllipsoid(center, sm::vec3(radius, radius, radius));
  }

  /// Setup quadric coefs to make a ellipsoid.
  void Quadric::InitAsEllipsoid(const sm::vec3& center, const sm::vec3& axisSize)
  {
    quadric = sm::mat4();
    quadric.m44 = -1;
    sm::mat4 m = sm::mat4::translation(center);
    m = m * sm::mat4::scale(axisSize.x, axisSize.y, axisSize.z);
    m = m.inverse();
    quadric = m.transpose() * quadric * m;
  }
  
  
  // -----------------------------------------------------

  /// Solve intersection ray vs. this object.
  bool Plane::Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats)
  {
    IntersectInfo interInfo;
    interInfo.objId = id;
    localStats.intersectionCount.PlusOne();
    sm::vec4 pos(r.pos, 1);
    sm::vec4 dir(r.dir, 0);
    FLOAT_TYPE t = -dot(coef_m, pos) / dot(coef_m, dir);
    intersectInterval.startingInside = false;
    if (t >= 0)
    {
      interInfo.t = t;
      interInfo.pos = r.pos + r.dir * t;
      sm::vec3 n = coef_m.xyz();
      interInfo.normal = normalize(sm::dot(r.dir, n) < 0 ? n : -n);
      interInfo.material = &material;
      intersectInterval.AddIntersection(interInfo);
      interInfo.normal = -interInfo.normal;
      intersectInterval.AddIntersection(interInfo);
      return true;
    }
    return false;
  }

  /// Prepare coefs with applied matrix.
  void Plane::Prepare()
  {
    Solid::Prepare();
    coef_m = matrixForNormals * coef;
  }
  
  // -----------------------------------------------------

  /// Solve intersection ray vs. this object.
  bool Rectangle::Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats)
  {
    bool res = Plane::Intersect(r, intersectInterval, localStats);
    if (res)
    {
      const int intersectionCount = (int)intersectInterval.intersections.size();
      if (intersectionCount == 0)
        return false;

      sm::vec3 p = intersectInterval.intersections[0].pos - pointA;
      //p = sm::normalize(p);
      FLOAT_TYPE u, v;
      
      u = sm::dot(p, a) / asize;
      v = sm::dot(p, b) / bsize;

      res = (u >= 0 && u <= 1 && v >= 0 && v <= 1);
      if (res)
        for (int i = 0; i < intersectionCount; i++)
        {
          intersectInterval.intersections[i].u = u;
          intersectInterval.intersections[i].v = v;
        }
    }
    return res;
  }

  /// Prepare coefs with applied matrix.
  void Rectangle::Prepare()
  {
    Plane::Prepare();
    pointA = (matrix * sm::vec4(pointA, 1)).xyz();
    a = (matrix * sm::vec4(a, 0)).xyz();
    b = (matrix * sm::vec4(b, 0)).xyz();
    asize = sm::length(a);
    bsize = sm::length(b);
    a = a * (1 / asize);
    b = b * (1 / bsize);
  }
  
  // -----------------------------------------------------
  
  /// Solve intersection ray vs. this object.
  bool HalfSpace::Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats)
  {
    IntersectInfo interInfo;
    interInfo.objId = id;
    interInfo.etaIn = material.eta;
    interInfo.etaOut = 1;
    localStats.intersectionCount.PlusOne();
    // n . x  + d = 0
    // n . (x_a + t * dir) + d = 0
    // n . x_a + t * n . dir + d = 0
    // t = - (n . x_a + d) / (n . dir)
    sm::vec4 pos(r.pos, 1);
    sm::vec4 dir(r.dir, 0);
    FLOAT_TYPE coef_m_DOT_pos = dot(coef_m, pos);
    FLOAT_TYPE t = -coef_m_DOT_pos / dot(coef_m, dir);
    intersectInterval.startingInside = coef_m_DOT_pos > 0;
    if (t > 0)
    {
      interInfo.t = t;
      interInfo.pos = r.pos + r.dir * t;
      sm::vec3 n = -coef_m.xyz();
      interInfo.normal = normalize(n);
      interInfo.material = &material;
      intersectInterval.AddIntersection(interInfo);
      return true;
    }
    
    return false;
  }

  // -----------------------------------------------------
  PointLight::~PointLight()
  {
    int lightCount = (int)scene->lights.size();
    PointLight** pLight = &scene->lights[0];
    for (int i = 0; i < lightCount; i++, pLight++)
      if (*pLight && (*pLight)->id == id)
      {
        scene->lights.erase(scene->lights.begin() + i);
        break;
      }
  }

  // -----------------------------------------------------
  void PointLight::OnCreate()
  {
    scene->lights.push_back(this);
    r = 1;
    g = 1;
    b = 1;
  }

  // -----------------------------------------------------
  void VolumeOperation::OnCreate()
  {
    first = NULL;
    second = NULL;
    mode = modeIntersect;
  }

  /// Prepare subtrees.
  void VolumeOperation::Prepare()
  {
    Solid* typedChild;
    Solid::Prepare();
    if (!first || !second)
      return;

    if (first->matrixBackupEmpty)
    {
      first->matrixBackup = first->matrix;
      first->matrixBackupEmpty = false;
    }
    first->matrix = matrix * first->matrixBackup;
    if (materialchanged && (typedChild = dynamic_cast<Solid*>(first)) != NULL)
    {
      typedChild->material = material;
      typedChild->materialchanged = true;
    }

    if (second->matrixBackupEmpty)
    {
      second->matrixBackup = second->matrix;
      second->matrixBackupEmpty = false;
    }
    second->matrix = matrix * second->matrixBackup;
    if (mode != modeMinus && materialchanged && (typedChild = dynamic_cast<Solid*>(second)) != NULL)
    {
      typedChild->material = material;
      typedChild->materialchanged = true;
    }

    first->Prepare();
    second->Prepare();
  }
  
  /// Solve intersection ray vs. this object.
  bool VolumeOperation::Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats)
  {
    if (!first || !second)
      return false;

    IntersectInterval ii1, ii2;
    first->Intersect(r, ii1, localStats);
    second->Intersect(r, ii2, localStats);  
    switch (mode)
    {
    case modeUnion:
      intersectInterval.MergeAorB(ii1, ii2, r);
      return intersectInterval.intersections.size() > 0;
    case modeIntersect:
      intersectInterval.MergeAandB(ii1, ii2, r, &material);
      return intersectInterval.intersections.size() > 0;
    case modeMinus:
      intersectInterval.MergeAminusB(ii1, ii2, r);
      return intersectInterval.intersections.size() > 0;
    default:;
    }
    return false;
  }
};

Scene::Scene(void)
{
  maxLevels = 1;
  stop = false;
  lightAttenuation.set((FLOAT_TYPE)1, (FLOAT_TYPE)2 / 50, (FLOAT_TYPE)1 / (50 * 50));
  maxDist = 100.0;
  backgroundColor.set(0.05, 0.05, 0.1);
  ambientLight.set(0.05, 0.05, 0.1);
}

Scene::~Scene(void)
{
  ClearAll();
}

bool Scene::IntersectAll(const sm::ray& r, IntersectInfo& intersectInfo, SceneStats& localStats)
{
  IntersectInterval tempIntersectInterval;

  bool rtn = false;
  int index;
  int count = activeNodes.size();
  node::SceneNode** pNode = &activeNodes[0];

  IntersectInterval unionInterval1;
  IntersectInterval unionInterval2;
  unionInterval1.Reset();
  unionInterval2.Reset();
  unionInterval1.intersections.reserve(count * 2);
  unionInterval2.intersections.reserve(count * 2);
  IntersectInterval* unionIntervalFrom = &unionInterval1;
  IntersectInterval* unionIntervalTo = &unionInterval2;
  for (index = 0; index < count; index++, pNode++)
  {
    if (stop) // quick break
      return false;
    node::SceneNode* node = *pNode;
    if (!node)
      continue;

    tempIntersectInterval.Reset();
    if (node->Intersect(r, tempIntersectInterval, localStats))
    {
      if (tempIntersectInterval.intersections.size())
      {
        rtn = true;
        IntersectInterval* swapVariable = unionIntervalFrom;
        unionIntervalFrom = unionIntervalTo;
        unionIntervalTo = swapVariable;
        unionIntervalTo->MergeAorB(tempIntersectInterval, *unionIntervalFrom, r);

        if (unionIntervalTo->intersections[0].t < intersectInfo.t)
          intersectInfo = unionIntervalTo->intersections[0];
      }
    }
  }
  
  if (unionIntervalTo->intersections.size() > 0)
  {
    intersectInfo = unionIntervalTo->intersections[0];
    return true;
  }
  
  return rtn;
}

sm::vec4 Scene::IntersectAllForLightAttenuation(const sm::ray& r, sm::vec4& startingLight, SceneStats& localStats)
{
  sm::vec3 normalizedToLight = sm::normalize(r.dir);
  IntersectInterval tempIntersectInterval;

  sm::vec4 rtn = startingLight;
  int index;
  int count = activeNodes.size();
  node::SceneNode** pNode = &activeNodes[0];

  IntersectInterval unionInterval1;
  IntersectInterval unionInterval2;
  unionInterval1.Reset();
  unionInterval2.Reset();
  unionInterval1.intersections.reserve(count * 2);
  unionInterval2.intersections.reserve(count * 2);
  IntersectInterval* unionIntervalFrom = &unionInterval1;
  IntersectInterval* unionIntervalTo = &unionInterval2;
  for (index = 0; index < count; index++, pNode++)
  {
    if (stop) // quick break
      return sm::vec4();
    node::SceneNode* node = *pNode;
    if (!node)
      continue;

    tempIntersectInterval.Reset();
    if (node->Intersect(r, tempIntersectInterval, localStats))
    {
      if (tempIntersectInterval.intersections.size())
      {
        IntersectInterval* swapVariable = unionIntervalFrom;
        unionIntervalFrom = unionIntervalTo;
        unionIntervalTo = swapVariable;
        unionIntervalTo->MergeAorB(*unionIntervalFrom, tempIntersectInterval, r);
      }
    }
  }
  
  int i = unionIntervalTo->intersections.size() - 1;
  IntersectInfo* pII = (i+1) ? &unionIntervalTo->intersections[0] : NULL;
  FLOAT_TYPE eta = 0;
  for (; i >= 0; i--, pII++)
  {
    if (pII->t >= 1)
      break;

    FLOAT_TYPE dirDotN = sm::dot(normalizedToLight, pII->normal);
    //if (dirDotN > 0)
    {
      if (pII->etaIn - node::ugly_eps > eta || pII->etaIn + node::ugly_eps < eta)
      {
        rtn = rtn * pII->material->transparent * (0.75 + 0.25 * abs(dirDotN));
      }

      eta = pII->etaIn;
      if (rtn.w < node::ugly_eps)
        break;
    }
    /*
    else
    {
      eta = 0;
    }
    */
  }
  
  return rtn;
}

sm::vec4 Scene::Raytrace(const sm::ray& r, int levels)
{
  if (levels == -1)
    levels = maxLevels;

  IntersectInfo intersectInfo;
  sm::vec4 color;
  color.w = 1;
  
  if (levels == 0 || nodes.size() == 0)
    return color;

  SceneStats localStats;
  if (IntersectAll(r, intersectInfo, localStats))
  {
    FLOAT_TYPE sign = sm::dot(intersectInfo.normal, r.dir) > 0 ? 1 : -1;
    sm::vec4 reflectIntensityCoef = sign < 0 ? intersectInfo.material->reflect : sm::vec4(0, 0, 0, 0);
    // refract
    if (intersectInfo.material->transparent.w > 0)
    {
      sm::ray r_refracted;
      r_refracted.pos = intersectInfo.pos /*+ sign * intersectInfo.normal * node::ugly_eps */+ r.dir * node::ugly_eps;

      FLOAT_TYPE etaFraction = intersectInfo.etaIn / intersectInfo.etaOut;
      etaFraction = sign > 0 ? etaFraction : 1 / etaFraction;

      r_refracted.dir = sm::refract(sm::normalize(r.dir), -sign * intersectInfo.normal, sign > 0 ? intersectInfo.material->eta : 1/intersectInfo.material->eta);
      if (sm::dot(r_refracted.dir, r_refracted.dir) < node::ugly_eps)
        reflectIntensityCoef = reflectIntensityCoef + intersectInfo.material->transparent;
      else
        color = color + intersectInfo.material->transparent * Raytrace(r_refracted, levels - 1);
    }

    // reflect
    if (reflectIntensityCoef.w > 0)
    {
      sm::ray r_reflected;
      r_reflected.pos = intersectInfo.pos - r.dir * node::ugly_eps;
      r_reflected.dir = sm::normalize(sm::reflect(r.dir, intersectInfo.normal));
      // dispersion
      if (intersectInfo.material->reflectionDispersion == 0)
      {
        color = color + reflectIntensityCoef * Raytrace(r_reflected, levels - 1);
      }
      else 
      {
        const int dispersionQuality = 32;
        const FLOAT_TYPE oneRayIntensity = (FLOAT_TYPE)1 / dispersionQuality;
        for (int i = 0; i < dispersionQuality; i++)
        {
          sm::ray r_reflected_d = r_reflected;
          r_reflected_d.dir = r_reflected_d.dir + intersectInfo.material->reflectionDispersion * FetchRandomGaussVector();
          color = color + oneRayIntensity * reflectIntensityCoef * Raytrace(r_reflected_d, levels - 1);
        }
      }
    }

    // local model
    sm::vec3 lightColor = LightsAll(r, intersectInfo, localStats);
    color = color + sm::vec4(lightColor.x, lightColor.y, lightColor.z, 0); 
    color.w = 1;
  }
  else
  {
    // background
    color.x = backgroundColor.x;
    color.y = backgroundColor.y;
    color.z = backgroundColor.z;
    color.w = 1;
  }

  globalStats.Join(localStats);
  
  return color;
}

sm::vec3 Scene::LightsAll(const sm::ray& r, const IntersectInfo& intersectInfo, SceneStats& localStats)
{
  sm::vec3 color;
  int lightindex = 0;
  int lightcount = activeLights.size();
  node::PointLight** pLight = lightcount ? &activeLights[0] : NULL;
  for (int lightindex = 0; lightindex < lightcount; lightindex++, pLight++)
  {
    if (stop)
      return color;
    node::PointLight* itLight = *pLight;

    sm::vec3 toLight(itLight->matrix.m14 - intersectInfo.pos.x,
      itLight->matrix.m24 - intersectInfo.pos.y,
      itLight->matrix.m34 - intersectInfo.pos.z);

    sm::ray rayToLight;
    rayToLight.pos = intersectInfo.pos - r.dir * node::ugly_eps;
    rayToLight.dir = toLight;

    sm::vec4 light = IntersectAllForLightAttenuation(rayToLight, sm::vec4(itLight->r, itLight->g, itLight->b, 1), localStats);
    if (light.w > (FLOAT_TYPE)0)
    {
      // NOT SHADOW
      const FLOAT_TYPE toLightLength2 = sm::length2(toLight);
      FLOAT_TYPE att = 1.0 / (lightAttenuation.x + lightAttenuation.y * sqrt(toLightLength2) + lightAttenuation.z * toLightLength2);

      if (intersectInfo.material->transparent.w > 0 && sm::dot(intersectInfo.normal, rayToLight.dir) < 0)
      {
        color = color + sm::phong(-intersectInfo.normal, -r.dir, toLight, ambientLight * intersectInfo.material->GetDiffuseRGB(intersectInfo.u, intersectInfo.v), 
          light.xyz() * intersectInfo.material->GetDiffuseRGB(intersectInfo.u, intersectInfo.v), light.xyz() * intersectInfo.material->GetReflectRGB(), intersectInfo.material->shininess) * att;
      }
      else
      {
        color = color + sm::phong(intersectInfo.normal, -r.dir, toLight, ambientLight * intersectInfo.material->GetDiffuseRGB(intersectInfo.u, intersectInfo.v), 
          light.xyz() * intersectInfo.material->GetDiffuseRGB(intersectInfo.u, intersectInfo.v), light.xyz() * intersectInfo.material->GetReflectRGB(), intersectInfo.material->shininess) * att;
      }
    }
    else
    {
      color = color + ambientLight * intersectInfo.material->GetDiffuseRGB(intersectInfo.u, intersectInfo.v);
      // SHADOW
    }
  }
  return color;
}

sm::vec4 Scene::Raytrace(FLOAT_TYPE u, FLOAT_TYPE v, int levels)
{
  if (levels == 0)
    return sm::vec4();

  sm::ray r = camera.GetRay(u, v);
  return Raytrace(r, levels);
}

void Scene::Prepare()
{
  globalStats.intersectionCount.Reset();

  for (int i = 0; i < GAUSSVEC_COUNT; i++)
    randomGaussVectors[i].set(sm::randomGauss(0, 0.1), sm::randomGauss(0, 0.1), sm::randomGauss(0, 0.1));

  int index;
  int count = activeNodes.size();
  if (!activeNodes.size())
    return;
  node::SceneNode** pNode = &activeNodes[0];
  for (index = 0; index < count; index++, pNode++)
  {
    node::SceneNode* node = *pNode;
    if (!node)
      continue;
    node->Prepare();
  }

  activeLights = lights;
}