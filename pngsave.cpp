#include "stdafx.h"

#pragma comment(lib, "lib/zlib.lib")
#pragma comment(lib, "lib/libpng.lib")

bool SaveBitmapToPNG(const char* filename, int width, int height, unsigned char* bitmap, char* errormsg)
{
  strcpy(errormsg, "");
  try
  {
    png::image<png::rgba_pixel> img;
    img.resize(width, height);

    for (int j = 0; j < height; j++)
    {
      for (int i = 0; i < width; i++)
      {
        const unsigned char* pixel = bitmap + (j * width + i) * 4;
        img.set_pixel(i, height - j - 1, png::rgba_pixel(pixel[0], pixel[1], pixel[2], pixel[3]));
      }
    }

    img.write(filename);
  }
  catch (...)
  {
    strcpy(errormsg, "Cannot save PNG image.");
    return false;
  }

  return true;
}