#include "stdafx.h"
#include "SceneLoader.h"

#include "ThreadWork.h"
#include "Scene.h"

#define TYP_VOID                           0
#define TYP_NEORIENTOVANA_ROVINA           3
#define TYP_POLOPROSTOR                    4
#define TYP_OBDELNIK                       5
#define TYP_KOULE_V_POCATKU                9
#define TYP_ELIPSOID_V_POCATKU            10
#define TYP_KOULE                         11
#define TYP_KVADR                         12
#define TYP_KVADRIKA                      15
#define TYP_RENDER                      9999

#define TYP_POSUV                         -1
#define TYP_ROTACE_OSA                    -2
#define TYP_ZMENA_MERITKA                 -3
#define TYP_OBECNA_TRANSFORMACE           -4

#define TYP_OP_SJEDNOCENI               -101
#define TYP_OP_PRUNIK                   -102
#define TYP_OP_MINUS                    -103
#define TYP_VLASTNOSTI                  -201
#define TYP_VLASTNOSTIRGB               -202
#define TYP_INDEX_LOMU                  -203
#define TYP_ROZMAZANI                   -204

#define TYP_SACHOVNICE_TEX              -399

#define TYP_BODOVE_SVETLO               -402

#define TYP_KAMERA                      -501
#define TYP_MONOKAMERA                  -502

#define TYP_BARVA                       -701
#define TYP_ODRAZOVABARVA               -702
#define TYP_PRUHLEDNOST                 -703

#define TYP_MAX_HLOUBKA                 -802
#define TYP_MIN_INT                     -803

enum TState
{
  stateId,
  stateType,
  stateValue
};

SceneLoader::SceneLoader(Scene* scene, ThreadWork* threadWork)
{
  anonymousObjectsIdCounter = -1;
  strcpy(err, "");
  this->scene = scene;
  this->threadWork = threadWork;
  InitTables();
}

SceneLoader::~SceneLoader(void)
{
}

void SceneLoader::InitTables()
{
  typeParamCount[TYP_VOID] = 0;
  typeParamCount[TYP_NEORIENTOVANA_ROVINA] = 4;
  typeParamCount[TYP_POLOPROSTOR] = 4;
  typeParamCount[TYP_OBDELNIK] = 4;
  typeParamCount[TYP_KOULE_V_POCATKU] = 1;
  typeParamCount[TYP_ELIPSOID_V_POCATKU] = 3;
  typeParamCount[TYP_KOULE] = 4;
  typeParamCount[TYP_KVADR] = 6;
  typeParamCount[TYP_KVADRIKA] = 16;

  typeParamCount[TYP_RENDER] = 4;

  typeParamCount[TYP_POSUV] = 3;
  typeParamCount[TYP_ROTACE_OSA] = 2;
  typeParamCount[TYP_ZMENA_MERITKA] = 3;
  typeParamCount[TYP_OBECNA_TRANSFORMACE] = 16;

  typeParamCount[TYP_BODOVE_SVETLO] = 5;
  typeParamCount[TYP_MAX_HLOUBKA] = 1;
  typeParamCount[TYP_OP_SJEDNOCENI] = 2;
  typeParamCount[TYP_OP_PRUNIK] = 2;
  typeParamCount[TYP_OP_MINUS] = 2;

  typeParamCount[TYP_BARVA] = 3;
  typeParamCount[TYP_ODRAZOVABARVA] = 3;
  typeParamCount[TYP_PRUHLEDNOST] = 3;

  typeParamCount[TYP_INDEX_LOMU] = 3;
  typeParamCount[TYP_ROZMAZANI] = 1;

  typeParamCount[TYP_KAMERA] = 10;
  typeParamCount[TYP_MONOKAMERA] = 1;

  typeParamCount[TYP_VLASTNOSTI] = 3;
  typeParamCount[TYP_VLASTNOSTIRGB] = 9;

  typeParamCount[TYP_MIN_INT] = 1;

  typeParamCount[TYP_SACHOVNICE_TEX] = 4;
}

bool SceneLoader::ProcessCommand()
{
  switch (tokType)
  {
  case TYP_VOID: 
  case TYP_MIN_INT:
    tokValues.clear(); return true;
  case TYP_NEORIENTOVANA_ROVINA: return Command_Neorientovana_Rovina();
  case TYP_POLOPROSTOR: return Command_Poloprostor();
  case TYP_KOULE_V_POCATKU: return Command_Elipsoid(0, tokValues[0]);
  case TYP_ELIPSOID_V_POCATKU: return Command_Elipsoid(0, sm::vec3(tokValues[0], tokValues[1], tokValues[2]));
  case TYP_KOULE: return Command_Elipsoid(sm::vec3(tokValues[0], tokValues[1], tokValues[2]), tokValues[3]);
  case TYP_KVADR: return Command_Kvadr();
  case TYP_OBDELNIK: return Command_Obdelnik();
  case TYP_KVADRIKA: return Command_Kvadrika();
  case TYP_RENDER: return Command_Render();
  case TYP_POSUV: return Command_Transformace(sm::mat4::translation(tokValues[0], tokValues[1], tokValues[2]));
  case TYP_ROTACE_OSA: 
    {
      int osa = (int)tokValues[1];
      if (osa == 1)
        return Command_Transformace(sm::mat4::rotationX(tokValues[0]));
      if (osa == 2)
        return Command_Transformace(sm::mat4::rotationY(tokValues[0]));
      if (osa == 3)
        return Command_Transformace(sm::mat4::rotationZ(tokValues[0]));

      strcpy(err, "Rotation: Invalid axis index.");
      return false;
    }
  case TYP_ZMENA_MERITKA: return Command_Transformace(sm::mat4::scale(tokValues[0], tokValues[1], tokValues[2]));
  case TYP_OBECNA_TRANSFORMACE: 
    {
      sm::mat4 m;
      for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
          m.m[j][i] = tokValues[i*4 + j];
      return Command_Transformace(m);
    }
  case TYP_BODOVE_SVETLO: return Command_Bodove_Svetlo();
  case TYP_MAX_HLOUBKA: return Command_Max_Hloubka();
  case TYP_OP_SJEDNOCENI: return Command_Op(opModeUnion);
  case TYP_OP_PRUNIK: return Command_Op(opModeIntersect);
  case TYP_OP_MINUS: return Command_Op(opModeMinus);
  case TYP_BARVA: return Command_Barva(0);
  case TYP_ODRAZOVABARVA: return Command_Barva(1);
  case TYP_PRUHLEDNOST: return Command_Barva(2);
  case TYP_INDEX_LOMU: return Command_Index_Lomu();
  case TYP_KAMERA: return Command_Kamera();
  case TYP_MONOKAMERA: return Command_MonoKamera();
  case TYP_ROZMAZANI: return Command_Rozmazani();
  case TYP_VLASTNOSTI: return Command_Barva(10);
  case TYP_VLASTNOSTIRGB: return Command_Barva(11);
  case TYP_SACHOVNICE_TEX: return Command_Sachovnice_Tex();
  default: sprintf(err, "Unknown command '%d'", (int)tokType); return false;
  }
}

bool SceneLoader::Command_Neorientovana_Rovina()
{
  node::Plane* plane = scene->CreateNode<node::Plane>(tokId);
  plane->coef.x = tokValues[0];
  plane->coef.y = tokValues[1];
  plane->coef.z = tokValues[2];
  plane->coef.w = tokValues[3];
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Poloprostor()
{
  node::HalfSpace* hs = scene->CreateNode<node::HalfSpace>(tokId);
  hs->coef.x = tokValues[0];
  hs->coef.y = tokValues[1];
  hs->coef.z = tokValues[2];
  hs->coef.w = tokValues[3];
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Obdelnik()
{
  node::Rectangle* rect = scene->CreateNode<node::Rectangle>(tokId);
  rect->pointA.set(tokValues[0], tokValues[1], 0);
  rect->a.set(tokValues[2] - tokValues[0], 0, 0);
  rect->b.set(0, tokValues[3] - tokValues[1], 0);
  rect->coef = sm::computePlaneCoef(rect->pointA, rect->pointA + rect->a, rect->pointA + rect->b);
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Elipsoid(sm::vec3 stred, sm::vec3 osyVelikost)
{
  node::Quadric* sphere = scene->CreateNode<node::Quadric>(tokId);
  sphere->InitAsEllipsoid(stred, osyVelikost);
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Kvadrika()
{
  node::Quadric* quadric = scene->CreateNode<node::Quadric>(tokId);
  quadric->quadric = sm::mat4(tokValues[0], tokValues[1], tokValues[2], tokValues[3], 
    tokValues[4], tokValues[5], tokValues[6], tokValues[7], 
    tokValues[8], tokValues[9], tokValues[10], tokValues[11], 
    tokValues[12], tokValues[13], tokValues[14], tokValues[15]);
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Kvadr()
{
  node::HalfSpace* hsLeft = scene->CreateNode<node::HalfSpace>(anonymousObjectsIdCounter--);
  hsLeft->coef.set(1, 0, 0, -min(tokValues[0], tokValues[3]));
  node::HalfSpace* hsBottom = scene->CreateNode<node::HalfSpace>(anonymousObjectsIdCounter--);
  hsBottom->coef.set(0, 1, 0, -min(tokValues[1], tokValues[4]));
  node::HalfSpace* hsBack = scene->CreateNode<node::HalfSpace>(anonymousObjectsIdCounter--);
  hsBack->coef.set(0, 0, 1, -min(tokValues[2], tokValues[5]));
  node::HalfSpace* hsRight = scene->CreateNode<node::HalfSpace>(anonymousObjectsIdCounter--);
  hsRight->coef.set(-1, 0, 0, max(tokValues[0], tokValues[3]));
  node::HalfSpace* hsTop = scene->CreateNode<node::HalfSpace>(anonymousObjectsIdCounter--);
  hsTop->coef.set(0, -1, 0, max(tokValues[1], tokValues[4]));
  node::HalfSpace* hsFront = scene->CreateNode<node::HalfSpace>(anonymousObjectsIdCounter--);
  hsFront->coef.set(0, 0, -1, max(tokValues[2], tokValues[5]));

  node::VolumeOperation* opPrev;
  node::VolumeOperation* op;
  
  op = scene->CreateNode<node::VolumeOperation>(anonymousObjectsIdCounter--);
  op->first = hsLeft;
  op->second = hsBottom;
  op->mode = node::VolumeOperation::modeIntersect;
  opPrev = op;

  op = scene->CreateNode<node::VolumeOperation>(anonymousObjectsIdCounter--);
  op->first = hsBack;
  op->second = opPrev;
  op->mode = node::VolumeOperation::modeIntersect;
  opPrev = op;

  op = scene->CreateNode<node::VolumeOperation>(anonymousObjectsIdCounter--);
  op->first = hsRight;
  op->second = opPrev;
  op->mode = node::VolumeOperation::modeIntersect;
  opPrev = op;

  op = scene->CreateNode<node::VolumeOperation>(anonymousObjectsIdCounter--);
  op->first = hsTop;
  op->second = opPrev;
  op->mode = node::VolumeOperation::modeIntersect;
  opPrev = op;

  op = scene->CreateNode<node::VolumeOperation>(tokId);
  op->first = hsFront;
  op->second = opPrev;
  op->mode = node::VolumeOperation::modeIntersect;

  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Render()
{
  node::Color* color = dynamic_cast<node::Color*>(scene->FindNode((int)tokValues[0]));
  if (color)
  {
    scene->backgroundColor.set(color->r, color->g, color->b);
  }
  else
  {
    scene->backgroundColor.set(0, 0, 0);
  }

  if ((int)tokValues[3] == -1 || tokValues[3] == (int)tokValues.size() - 4)
  {
    if (tokValues[3] == -1)
    {
      scene->activeNodes = scene->nodes;
      scene->activeLights = scene->lights;
    }
    else
    { 
      for (int i = 4; i < (int)tokValues.size(); i++)
      {
        scene->MarkAsActive((int)tokValues[i]);
      }
    }

    threadWork->Perform();
    tokValues.clear();
    typeParamCount[TYP_RENDER] = 4;
  }
  else if ((int)tokValues.size() == 4)
  {
    typeParamCount[TYP_RENDER] = 4 + (int)tokValues[3];
  }
  return true;
}

bool SceneLoader::Command_Transformace(const sm::mat4& transformace)
{
  node::SceneNode* node = scene->FindNode(tokId);
  node->matrix = transformace * node->matrix;
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Bodove_Svetlo()
{
  node::PointLight* light = scene->CreateNode<node::PointLight>(tokId);
  light->r = tokValues[4];
  light->g = tokValues[4];
  light->b = tokValues[4];
  light->matrix = sm::mat4::translation(tokValues[0], tokValues[1], tokValues[2]);
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Max_Hloubka()
{
  scene->maxLevels = (int)tokValues[0];
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Barva(int typeIndex)
{
  node::SceneNode* node = scene->FindNode(tokId);
  node::Solid* solid = dynamic_cast<node::Solid*>(node);
  if (!solid)
  {
    node::Color* color = scene->CreateNode<node::Color>(tokId);
    //strcpy(err, "Cannot apply material on object."); // not an error
    color->r = tokValues[0];
    color->g = tokValues[1];
    color->b = tokValues[2];
    tokValues.clear();
    return true;
  }

  solid->materialchanged = true;

  switch (typeIndex)
  {
  case 0:
    solid->material.diffuse.x *= (FLOAT_TYPE)tokValues[0];
    solid->material.diffuse.y *= (FLOAT_TYPE)tokValues[1];
    solid->material.diffuse.z *= (FLOAT_TYPE)tokValues[2];
    break;
  case 1:
    if (solid->material.reflect.w == 0)
    {
      solid->material.reflect.x = (FLOAT_TYPE)tokValues[0];
      solid->material.reflect.y = (FLOAT_TYPE)tokValues[1];
      solid->material.reflect.z = (FLOAT_TYPE)tokValues[2];
      solid->material.reflect.w = (solid->material.reflect.x + solid->material.reflect.y + solid->material.reflect.z > 0) ? 1 : 0;
    }
    else
    {
      solid->material.reflect.x *= (FLOAT_TYPE)tokValues[0];
      solid->material.reflect.y *= (FLOAT_TYPE)tokValues[1];
      solid->material.reflect.z *= (FLOAT_TYPE)tokValues[2];
      solid->material.reflect.w = (solid->material.reflect.x + solid->material.reflect.y + solid->material.reflect.z > 0) ? 1 : 0;
    }
    break;
  case 2:
    if (solid->material.transparent.w == 0)
    {
      solid->material.transparent.x = (FLOAT_TYPE)tokValues[0];
      solid->material.transparent.y = (FLOAT_TYPE)tokValues[1];
      solid->material.transparent.z = (FLOAT_TYPE)tokValues[2];
      solid->material.transparent.w = (solid->material.transparent.x + solid->material.transparent.y + solid->material.transparent.z > 0) ? 1 : 0;
    }
    else
    {
      solid->material.transparent.x *= (FLOAT_TYPE)tokValues[0];
      solid->material.transparent.y *= (FLOAT_TYPE)tokValues[1];
      solid->material.transparent.z *= (FLOAT_TYPE)tokValues[2];
      solid->material.transparent.w = (solid->material.transparent.x + solid->material.transparent.y + solid->material.transparent.z > 0) ? 1 : 0;
    }
    break;
  case 10:
    {
      double d, s, t;
      d = tokValues[0];
      s = tokValues[1];
      t = tokValues[2];
      tokValues.clear();
      tokValues.push_back(d);
      tokValues.push_back(d);
      tokValues.push_back(d);
      Command_Barva(0);
      tokValues.push_back(s);
      tokValues.push_back(s);
      tokValues.push_back(s);
      Command_Barva(1);
      tokValues.push_back(t);
      tokValues.push_back(t);
      tokValues.push_back(t);
      Command_Barva(2);
    }
    break;
  case 11:
    {
      double d[3], s[3], t[3];
      d[0] = tokValues[0];
      s[0] = tokValues[1];
      t[0] = tokValues[2];
      d[1] = tokValues[3];
      s[1] = tokValues[4];
      t[1] = tokValues[5];
      d[2] = tokValues[6];
      s[2] = tokValues[7];
      t[2] = tokValues[8];
      tokValues.clear();
      tokValues.push_back(d[0]);
      tokValues.push_back(d[1]);
      tokValues.push_back(d[2]);
      Command_Barva(0);
      tokValues.push_back(s[0]);
      tokValues.push_back(s[1]);
      tokValues.push_back(s[2]);
      Command_Barva(1);
      tokValues.push_back(t[0]);
      tokValues.push_back(t[1]);
      tokValues.push_back(t[2]);
      Command_Barva(2);
    }
    break;
  }
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Index_Lomu()
{
  node::SceneNode* node = scene->FindNode(tokId);
  node::Solid* solid = dynamic_cast<node::Solid*>(node);
  if (!solid)
  {
    strcpy(err, "Cannot apply material on object.");
    return false;
  }
  solid->material.eta = (FLOAT_TYPE)tokValues[0];
  solid->materialchanged = true;
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Rozmazani()
{
  node::SceneNode* node = scene->FindNode(tokId);
  node::Solid* solid = dynamic_cast<node::Solid*>(node);
  if (!solid)
  {
    strcpy(err, "Cannot apply material on object.");
    return false;
  }
  solid->material.reflectionDispersion = (FLOAT_TYPE)tokValues[0];
  solid->materialchanged = true;
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Sachovnice_Tex()
{
  node::SceneNode* node = scene->FindNode(tokId);
  node::Solid* solid = dynamic_cast<node::Solid*>(node);
  if (!solid)
  {
    strcpy(err, "Cannot generate checkerboard, solid object not found.");
    return false;
  }
  int idcol1 = (int)tokValues[2];
  int idcol2 = (int)tokValues[3];
  node::Color* color1 = dynamic_cast<node::Color*>(scene->FindNode(idcol1));
  node::Color* color2 = dynamic_cast<node::Color*>(scene->FindNode(idcol2));
  if (!color1 || !color2)
  {
    strcpy(err, "Cannot generate checkerboard, color object not found.");
    return false;
  }
  solid->material.texture.GenerateCheckerboard((int)tokValues[0], (int)tokValues[1], sm::vec4(color1->r, color1->g, color1->b, 1), sm::vec4(color2->r, color2->g, color2->b, 1));
  solid->materialchanged = true;
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Kamera()
{
  node::Camera* camera = scene->CreateNode<node::Camera>(tokId);
  camera->Setup(sm::vec3(tokValues[0], tokValues[1], tokValues[2]),
    sm::vec3(tokValues[3], tokValues[4], tokValues[5]),
    sm::vec3(tokValues[6], tokValues[7], tokValues[8]), tokValues[9]);
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_MonoKamera()
{
  node::SceneNode* node = scene->FindNode((int)tokValues[0]);
  node::Camera* camera = dynamic_cast<node::Camera*>(node);
  if (!camera)
  {
    strcpy(err, "Invalid mono camera.");
    return false;
  }

  scene->camera = *camera;
  tokValues.clear();
  return true;
}

bool SceneLoader::Command_Op(OPMODE opMode)
{
  node::SceneNode* first = scene->FindNode((int)tokValues[0]);
  node::SceneNode* second = scene->FindNode((int)tokValues[1]);
  if (!first || !second)
  {
    strcpy(err, "Volume operations: Solid not found.");
    return false;
  }
  node::VolumeOperation* op = scene->CreateNode<node::VolumeOperation>(tokId);
  op->first = first;
  op->second = second;
  switch (opMode)
  {
  case opModeUnion: op->mode = node::VolumeOperation::modeUnion; break;
  case opModeIntersect: op->mode = node::VolumeOperation::modeIntersect; break;
  case opModeMinus: op->mode = node::VolumeOperation::modeMinus; break;
  default: strcpy(err, "Volume operations: Unsupported operation."); return false;
  }
  tokValues.clear();
  return true;
}

bool SceneLoader::Load(const char* filename)
{
  FILE* f = fopen(filename, "r");
  if (!f)
  {
    strcpy(err, "Cannot open file ");
    strcat(err, filename);
    return false;
  }

  TState state = stateId;

  bool rtn = true;
  char* token = (char*)malloc(4096 * sizeof(char));
  bool gotonextline;
  while (rtn && fscanf(f, "%s", token) == 1)
  {
    gotonextline = false;
    char* commentStart = strchr(token, '#');
    if (commentStart)
    {
      *commentStart = '\0';
      gotonextline = true;
    }

    switch (state)
    {
    case stateId: 
      if (sscanf(token, "%d", &tokId) == 1)
      {
        state = stateType;
        if ((int)tokId == 0)
        {
          tokType = TYP_VOID;
          state = stateId;
        }
      }
      else
      {
        tokType = TYP_VOID;
      }
      break;
    case stateType: 
      if (sscanf(token, "%d", &tokType) == 1)
      {
        state = stateValue; 
      }
      break;
    case stateValue:
      if ((int)tokValues.size() < typeParamCount[tokType])
      {
        double d = 0.0;
        if (sscanf(token, "%lf", &d) == 1)
        {
          tokValues.push_back(d);
        }
      }

      int tokSize = (int)tokValues.size();
      if (tokSize == typeParamCount[tokType])
      {
        rtn = ProcessCommand();
        if (tokValues.size() == 0) // has param count been changed?
          state = stateId;
      }
      break;
    }

    if (gotonextline)
    {
      char c;
      do { c = fgetc(f); } while (c != '\n' && c != EOF);
    }
  }

  if (rtn && state != stateId)
  {
    strcpy(err, "Unexpected end of file.");
    rtn = false;
  }

  free(token);
  fclose(f);
  return rtn;
}
