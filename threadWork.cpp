#include "stdafx.h"
#include "ThreadWork.h"
#include "raytracer.h"

ThreadWork::ThreadWork(Scene* sc, SimpleDrawing* sd)
{
  this->scene = sc;
  this->simpleDraw = sd;
  this->perform = false;
  this->performed = false;
  mutexFetchWork = CreateMutex(NULL, false, NULL);
  threadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)&ThreadProc, this, 0, NULL);
  renderTimeMs = 0;
}

ThreadWork::~ThreadWork(void)
{
  CloseHandle(mutexFetchWork);

  if (threadHandle)
  {
    terminated = true;
    StopRaytracer();
    WaitForSingleObject(threadHandle, INFINITE);
  }
}

/// Thread entry function.
DWORD ThreadWork::ThreadProc(LPVOID lpdwThreadParam)
{
  ((ThreadWork*)lpdwThreadParam)->Run();
  return 0;
}

DWORD ThreadWork::WorkerProc(LPVOID lpdwThreadParam)
{
  rect_t r;
  while (((ThreadWork*)lpdwThreadParam)->FetchWork(r))
  {
    ((ThreadWork*)lpdwThreadParam)->ProcessRaytracer(r);
  }
  return 0;
}

/// Run the ThreadWork.
void ThreadWork::Run()
{
  terminated = false;
  while (!terminated)
  {
    if (!perform)
    {
      Sleep(100);
      continue;
    }
    
    DWORD started = GetTickCount();
    // prepare work
    InitWork();

    // create workers
    DWORD numThreads = (DWORD)WorkerCount();
    simpleDraw->ClearColor(0.6, 0.7, 1.0, 0.0);
    for (DWORD i = 0; i < numThreads; i++)
      workers.push_back(CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)&WorkerProc, this, 0, NULL));

    // wait for completion
    WaitForMultipleObjects((DWORD)workers.size(), &workers[0], true, INFINITE);
    workers.clear();

    DWORD ended = GetTickCount();
    renderTimeMs = ended - started;

    perform = false;
    performed = true;

    ImageRendered();
  }
}

/// ThreadWork initialization.
void ThreadWork::InitWork()
{
  scene->Prepare();

  rectWork.clear();
  
  int w = simpleDraw->GetBufferWidth();
  int h = simpleDraw->GetBufferHeight();
  /*
  for (int j = (3*h/5); j < (4*h/5); j += WORK_PIECE_SIZE)
    for (int i = (0*w/5); i < (5*w/5); i += WORK_PIECE_SIZE)
      */
  for (int j = 0; j < h; j += WORK_PIECE_SIZE)
    for (int i = 0; i < w; i += WORK_PIECE_SIZE)
    {
      rect_t r;
      r.x1 = i;
      r.y1 = j;
      r.x2 = min(i + WORK_PIECE_SIZE - 1, w);
      r.y2 = min(j + WORK_PIECE_SIZE - 1, h);
      rectWork.push_back(r);
    }

  int count = rectWork.size();
  for (int i = 0; i < count; i++)
  {
    int i1 = rand() % count;
    int i2 = rand() % count;
    rect_t r_swap;
    r_swap = rectWork[i1];
    rectWork[i1] = rectWork[i2];
    rectWork[i2] = r_swap;
  }

  indexWork = count - 1;
}

/// Fetch work for one thread.
bool ThreadWork::FetchWork(rect_t& r)
{
  bool rtn = false;
  WaitForSingleObject(mutexFetchWork, INFINITE);
  if (indexWork >= 0)
  {
    r = rectWork[indexWork];
    indexWork--;
    rtn = true;
  }
  ReleaseMutex(mutexFetchWork);
  return rtn;
}

#ifndef _DEBUG
#define RAYTRACER_ANTIALIASING 1
#endif

// --------------------------------------------------------------------------------
void ThreadWork::ProcessRaytracer(const rect_t& r)
{
  int w = simpleDraw->GetBufferWidth();
  int h = simpleDraw->GetBufferHeight();
  const FLOAT_TYPE aspect = (FLOAT_TYPE)h / w;
  
#ifdef _DEBUG
  int debugStep = 5;
  for (int j = r.y1; j <= r.y2; j += debugStep)
  {
    for (int i = r.x1; i <= r.x2; i += debugStep)
    {
      sm::vec4 color;     
      color = scene->Raytrace(((FLOAT_TYPE)i) / w, (1 - aspect) * 0.5 + aspect * ((FLOAT_TYPE)j) / h);
      for (int j1 = j; j1 < j + debugStep && j1 <= r.y2; j1++)
        for (int i1 = i; i1 < i + debugStep && i1 <= r.x2; i1++)
        {
          simpleDraw->PutPixel(i1, j1, color.x, color.y, color.z, color.w);
        }
    }
  }
#else
  for (int j = r.y1; j <= r.y2; j++)
  {
    for (int i = r.x1; i <= r.x2; i++)
    {
      sm::vec4 aColor;
#if RAYTRACER_ANTIALIASING != 1
      aColor = scene->Raytrace(((FLOAT_TYPE)i) / w, (1 - aspect) * 0.5 + aspect * ((FLOAT_TYPE)j) / h);
#else
      /*prepared for antialiasing*/
      
      aColor = aColor + scene->Raytrace(((FLOAT_TYPE)i) / w, (1 - aspect) * 0.5 + aspect * ((FLOAT_TYPE)j) / h);
      aColor = aColor + scene->Raytrace(((FLOAT_TYPE)i + 0.5) / w, (1 - aspect) * 0.5 + aspect * ((FLOAT_TYPE)j) / h);
      aColor = aColor + scene->Raytrace(((FLOAT_TYPE)i + 0.5) / w, (1 - aspect) * 0.5 + aspect * ((FLOAT_TYPE)j + 0.5) / h);
      aColor = aColor + scene->Raytrace(((FLOAT_TYPE)i) / w, (1 - aspect) * 0.5 + aspect * ((FLOAT_TYPE)j + 0.5) / h);
      aColor = aColor * 0.25f;
      sm::vec4 backargb(scene->backgroundColor.x, scene->backgroundColor.y, 
        scene->backgroundColor.z, 0);
      if (aColor.w < 1)
        aColor = backargb;
#endif 
      simpleDraw->PutPixel(i, j, aColor.x, aColor.y, aColor.z, aColor.w);
    }
  }
#endif
}

void ThreadWork::GetProcessInfo(char* strInfo)
{
  if (perform)
  {
    sprintf(strInfo, "[rendering using %d processor core%s]", (int)workers.size(), (int)workers.size() > 1 ? "s" : "");
    return;
  }
  else if (!performed)
  {
    strcpy(strInfo, "");
  }
  else
  {
    char str_intersections[512];
    scene->GetTestedIntersectionCount().ToString(str_intersections);

    float seconds = (renderTimeMs % 60000) * 0.001f;
    int minutes = renderTimeMs / 60000;
    if (minutes)
      sprintf(strInfo, "[Render time: %d min %.2f sec] [tested %s intersections]", minutes, seconds, str_intersections);
    else
      sprintf(strInfo, "[Render time: %.2f sec] [tested %s intersections]", seconds, str_intersections);
  }
}

/// Get info about process.
void ThreadWork::GetFullProcessInfo(char* strInfo)
{
  if (perform)
  {
    sprintf(strInfo, "Rendering using %d processor core%s...", (int)workers.size(), (int)workers.size() > 1 ? "s" : "");
    return;
  }
  else if (!performed)
  {
    strcpy(strInfo, "No statistics have been collected yet.");
  }
  else
  {
    char str_intersections[512];
    scene->GetTestedIntersectionCount().ToString(str_intersections);

    float seconds = (renderTimeMs % 60000) * 0.001f;
    int minutes = renderTimeMs / 60000;
    if (minutes)
      sprintf(strInfo, "Render time: %d min %.2f sec\nTested %s intersections", minutes, seconds, str_intersections);
    else
      sprintf(strInfo, "Render time: %.2f sec\nTested %s intersections", seconds, str_intersections);
  }
}