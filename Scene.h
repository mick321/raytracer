#pragma once
#include "simplemath.h"

class Scene;

namespace node
{
  class Material;
}

/// Intersection and its description.
/// Contains position, normal, color and parameter t of ray.
class IntersectInfo
{
public:
  sm::vec3 pos;
  sm::vec3 normal;
  node::Material* material;
  FLOAT_TYPE t;
  int objId;

  FLOAT_TYPE u, v; ///< texture coords

  FLOAT_TYPE etaIn; ///< index of refraction inside intersected solid 
  FLOAT_TYPE etaOut; ///< index of refraction outside intersected solid 
  bool switchInside;

  IntersectInfo()
  {
    Reset();
  }

  /*
  IntersectInfo(const IntersectInfo& copyFrom)
  {
    this->pos = copyFrom.pos;
    this->normal = copyFrom.normal;
    this->material = copyFrom.material; // SHALLOW COPY! (copying only pointer)
    this->t = copyFrom.t;
    this->objId = copyFrom.objId;
  }
  */

  void Reset()
  {
    material = NULL;
    t = 1e30;
    switchInside = true;
  }
};

/// "Array" of intersections (interval).
struct IntersectInterval
{
  std::vector<IntersectInfo> intersections;
  bool startingInside;
  FLOAT_TYPE startingEta;

  IntersectInfo& AddIntersection(const IntersectInfo& ii)
  {
    int k = 0;
    while (k < (int)intersections.size())
    {
      if (ii.t < intersections[k].t)
        break;
      k++;
    }
        
    intersections.insert(intersections.begin() + k, ii);
    return intersections[k];
  }

  /// Reset object, clear stored infromation.
  void Reset()
  {
    startingInside = false;
    intersections.clear();
  }

  /// Merge two intersectintervals into this using (A and B).
  void MergeAandB(IntersectInterval& A, IntersectInterval& B, const sm::ray& ray, node::Material* newMaterial);
  /// Merge two intersectintervals into this using (A or B implemented as (A or (B - A))).
  void MergeAorB(IntersectInterval& A, IntersectInterval& B, const sm::ray& ray);
  /// Merge two intersectintervals into this using (A minus B).
  void MergeAminusB(IntersectInterval& A, IntersectInterval& B, const sm::ray& ray);
};

#define LARGE_NUMBER_COUNTER_N 10
struct LARGE_NUMBER_COUNTER
{
  int num[LARGE_NUMBER_COUNTER_N];
  long lock;
  LARGE_NUMBER_COUNTER()
  {
    Reset();
  }

  void Reset()
  {
    lock = 0;
    for (int i = 0; i < LARGE_NUMBER_COUNTER_N; i++)
    {
      num[i] = 0;
    }
  }

  inline void PlusOne()
  { 
    int* pnum = num + LARGE_NUMBER_COUNTER_N - 1;
    (*pnum)++;
    for (; pnum > num; pnum--)
    {
      if (*pnum == 1000000000)
      {
        *pnum = 0;
        (*(pnum-1))++;
      }
    }
  }

  inline void InterlockedPlus(LARGE_NUMBER_COUNTER& rhs)
  {
    while (InterlockedExchange(&lock, 1))
    {
      // wait
    }

    int* pnum = num + LARGE_NUMBER_COUNTER_N - 1;
    int* rhs_pnum = rhs.num + LARGE_NUMBER_COUNTER_N - 1;    
    for (; pnum > num; pnum--, rhs_pnum--)
    {
      (*pnum) += (*rhs_pnum);
      if ((*pnum) > 1000000000)
      {
        (*pnum) -= 1000000000;
        (*(pnum-1))++;
      }
    }

    lock = 0;
  }

  void ToString(char* str) const
  {
    bool output = false;
    int tick = 3;
    for (int i = 0; i < LARGE_NUMBER_COUNTER_N; i++)
    {
      for (int divisor = 100000000; divisor >= 1; divisor /= 10)
      {
        int part = (num[i] / divisor) % 10;
        if (part)
          output = true;

        tick--;
        if (output)
        {
          *str = '0' + part;
          str++;
          if (tick == 0 && (i < LARGE_NUMBER_COUNTER_N - 1 || divisor > 1))
          {
            *str = ',';
            str++;
          }
        }
        if (tick == 0)
          tick = 3;
      }
    }

    *(str) = '\0';
  }
};


/// Performance statistics
struct SceneStats
{
  LARGE_NUMBER_COUNTER intersectionCount;
  void Join(SceneStats& addTheseStats)
  {
    intersectionCount.InterlockedPlus(addTheseStats.intersectionCount);
  }
};

namespace node
{

  ///
  /// Obecny uzel sceny.
  ///
  class SceneNode
  {
  private:
    friend class Scene;
  protected:    
  public:
    sm::mat4   matrixBackup;
    bool       matrixBackupEmpty;

    sm::mat4   matrix;
    sm::mat4   matrixForNormals;
    sm::mat4   inverseMatrix;
    sm::mat4   inverseMatrixForNormals;
    Scene*     scene;
    int        id; ///< obj id

    SceneNode()
    {
      matrixBackupEmpty = true;
    }

    virtual ~SceneNode()
    {
      
    }

    virtual void Prepare()
    {
      inverseMatrix = matrix.inverse();
      matrixForNormals = inverseMatrix.transpose();
      inverseMatrixForNormals = matrixForNormals.inverse();
    }

    /// Solve intersection ray vs. this object.
    virtual bool Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats) { return false; }

    virtual void OnCreate() { }
  };

  ///
  /// Texture.
  ///
  class Texture
  {
  private:
    /// texture pixels
    std::vector<sm::vec4> pixels;
    int sizex, sizey;
  public:
    Texture()
    {
      sizex = 0;
      sizey = 0;
    }
    /// Generate checkerboard texture.
    void GenerateCheckerboard(int m, int n, const sm::vec4& dark, const sm::vec4& light)
    {
      sizex = m;
      sizey = n;
      pixels.resize(m * n);
      for (int j = 0; j < n; j++)
        for (int i = 0; i < m; i++)
        {
          pixels[j * m + i] = ((i+j)%2) ? light : dark;
        }
    }
    /// Sample texel, texture is repeating.
    sm::vec4 Sample(FLOAT_TYPE u, FLOAT_TYPE v)
    {
      if (sizex == 0)
        return sm::vec4(1);

      FLOAT_TYPE intpart1, intpart2;
      int samplex = (int)(modf(u, &intpart1) * sizex);
      int sampley = (int)(modf(v, &intpart2) * sizey);
      return pixels[sampley * sizex + samplex];
    }
  };

  ///
  /// Material.
  ///
  class Material
  {
  public:
    /// Stored diffuse color - RGBA.
    sm::vec4 diffuse;
    /// Material shininess.
    FLOAT_TYPE shininess;
    /// Reflect (specular) color.
    sm::vec4 reflect;
    /// Transparent color (multiplier of light that passes through).
    sm::vec4 transparent;
    /// Opacity color = 1 - Transparent
    sm::vec4 opacity;
    /// Coef eta, that is index of refraction.
    FLOAT_TYPE eta; 
    /// Dispersion of reflected rays.
    FLOAT_TYPE reflectionDispersion;
    /// Used texture.
    Texture texture;
    
    /// Default constructor.
    Material()
    {
      diffuse.x = 1;
      diffuse.y = 1;
      diffuse.z = 1;
      diffuse.w = 1;
      reflect.x = 0;
      reflect.y = 0;
      reflect.z = 0;
      reflect.w = 0;
      transparent.x = 0;
      transparent.y = 0;
      transparent.z = 0;
      transparent.w = 0;
      shininess = 50;
      eta = 1;
      reflectionDispersion = 0;
    }
    /// Get only three components of diffuse (R, G, B).
    inline const sm::vec3 GetDiffuseRGB(const FLOAT_TYPE u, const FLOAT_TYPE v) { return sm::vec3(diffuse.x, diffuse.y, diffuse.z) * texture.Sample(u, v).xyz(); }
    /// Get only three components of reflect (R, G, B).
    inline const sm::vec3 GetReflectRGB() { return sm::vec3(reflect.x, reflect.y, reflect.z); }
    /// Get only three components of transparent (R, G, B).
    inline const sm::vec3 GetTransparentRGB() { return sm::vec3(transparent.x, transparent.y, transparent.z); }
  };

  ///
  /// Color.
  ///
  class Color : public SceneNode
  {
  public:
    FLOAT_TYPE r, g, b;
    /// Konstruktor.
    Color()
    {
      r = 1;
      g = 1;
      b = 1;
    }
  };

  ///
  /// Camera representation.
  ///
  class Camera : public SceneNode
  {
  public:
    /*
      very simple representation for raytracer:
      S is ray source
      A,B,C,(D) represent projection plane

                A
             ...|\D
          ...   | |
       ...      | |
     S*...      | |
          ...   | |
             ...| |
                B\|
                  C
    */
    sm::vec3 source;
    sm::vec3 topLeft, topRight, bottomLeft;

    /// constructor
    Camera();

    /// get ray
    sm::ray GetRay(FLOAT_TYPE u, FLOAT_TYPE v);

    void Setup(const sm::vec3& position, const sm::vec3& direction, const sm::vec3& upDirection, const FLOAT_TYPE fov);
  };

  ///
  /// Abstract solid representation.
  ///
  class Solid : public SceneNode
  {
  public:
    bool materialchanged;
    Material material;

    Solid() { }

    virtual void Prepare();
    virtual void OnCreate();
  };

  ///
  /// Quadric representation.
  ///
  class Quadric : public Solid
  {
  public:
    sm::mat4 quadric; ///< description of quadric
    sm::mat4 quadric_m; ///< description of quadric with applied transformations
    sm::mat4 quadric_m_t; ///< transposed description of quadric with applied transformations
    /// Solve intersection ray vs. this object.
    virtual bool Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats);
    /// Prepare coefs with applied matrix.
    virtual void Prepare();

    /// Setup quadric coefs to make a sphere.
    void InitAsSphere(const sm::vec3& center, const FLOAT_TYPE radius);
    /// Setup quadric coefs to make a ellipsoid.
    void InitAsEllipsoid(const sm::vec3& center, const sm::vec3& axisSize);

  };

  ///
  /// Plane representation.
  ///
  class Plane : public Solid
  {
  public:
    sm::vec4 coef;
    sm::vec4 coef_m; ///< coef with applied matrix
    /// Solve intersection ray vs. this object.
    virtual bool Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats);
    /// Prepare coefs with applied matrix.
    virtual void Prepare();
  };

  ///
  /// Rectangle representation.
  ///
  class Rectangle : public Plane
  {
  public:
    sm::vec3 pointA;
    sm::vec3 a, b;
    FLOAT_TYPE asize, bsize;
    /// Solve intersection ray vs. this object.
    virtual bool Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats);
    /// Prepare coefs with applied matrix.
    virtual void Prepare();
  };

  ///
  /// Half-space representation.
  ///
  class HalfSpace : public Plane
  {
  public:
    /// Solve intersection ray vs. this object.
    virtual bool Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats);
  };

  ///
  /// Point light representation.
  ///
  class PointLight : public SceneNode
  {
  public:
    FLOAT_TYPE r, g, b;
    ~PointLight();

    virtual void OnCreate();
  };

  ///
  /// Volume (set) operations.
  ///
  class VolumeOperation : public Solid
  {
  public:
    SceneNode* first;
    SceneNode* second;
    enum { modeUnion, modeIntersect, modeMinus } mode;
    
    virtual void OnCreate();
    /// Solve intersection ray vs. this object.
    virtual bool Intersect(const sm::ray& r, IntersectInterval& intersectInterval, SceneStats& localStats);

    /// Prepare subtrees.
    virtual void Prepare();
  };

  const FLOAT_TYPE ugly_eps = (FLOAT_TYPE)1/(1024*1024);
}

#define GAUSSVEC_COUNT 1000

///
/// Reprezentace sceny.
///
class Scene
{
private:
  sm::vec3 randomGaussVectors[GAUSSVEC_COUNT];
  int randomGaussVectorsIndex;

  bool IntersectAll(const sm::ray& r, IntersectInfo& intersectInfo, SceneStats& localStats);
  sm::vec4 IntersectAllForLightAttenuation(const sm::ray& r, sm::vec4& startingLight, SceneStats& localStats);
  sm::vec3 LightsAll(const sm::ray& r, const IntersectInfo& intersectInfo, SceneStats& localStats);
public:

  inline const sm::vec3& FetchRandomGaussVector()
  {
    /*
    randomGaussVectorsIndex++;
    if (randomGaussVectorsIndex * 2 > GAUSSVEC_COUNT)
      randomGaussVectorsIndex = rand() % (GAUSSVEC_COUNT / 2);
    */
    return randomGaussVectors[rand() % (GAUSSVEC_COUNT)];
  }

  FLOAT_TYPE maxDist;
  sm::vec3 ambientLight;
  sm::vec3 lightAttenuation;
  sm::vec3 backgroundColor;
  int maxLevels;
  SceneStats globalStats;
  bool stop;

  std::vector<node::PointLight*>     lights;
  std::vector<node::SceneNode*>      nodes;
  node::Camera                       camera;

  std::vector<node::PointLight*>     activeLights;
  std::vector<node::SceneNode*>      activeNodes;

  /// Defaukt constructor.
  Scene(void);
  /// Defaukt destructor.
  ~Scene(void);

  void Prepare();

  sm::vec4 Raytrace(FLOAT_TYPE u, FLOAT_TYPE v, int levels = -1);
  sm::vec4 Raytrace(const sm::ray& r, int levels);

  inline const LARGE_NUMBER_COUNTER& GetTestedIntersectionCount() const { return globalStats.intersectionCount; }

  // ----
  template <typename NODETYPE>
  NODETYPE* CreateNode(int id) 
  {
    NODETYPE* node = new NODETYPE();
    node->id = id;
    node->scene = this;
    nodes.push_back(node);
    node->OnCreate();
    return node;
  }

  // ----
  node::SceneNode* FindNode(int id, int* _index = NULL)
  {
    int count = (int)nodes.size();
    if (!count)
      return NULL;

    node::SceneNode** pNode = &nodes[0];
    for (int i = 0; i < count; i++, pNode++)
      if ((*pNode) && (*pNode)->id == id)
      {
        if (_index)
          *_index = i;
        return (*pNode);
      }

    return NULL;
  }

  // ----
  bool DeleteNode(int id)
  { 
    int index;
    node::SceneNode* node = FindNode(id, &index);
    if (node)
    {
      nodes.erase(nodes.begin() + index);
      delete node;
      return true;
    }
    return false;
  }

  // ----
  void ClearAll()
  {
    activeNodes.clear();
    activeLights.clear();

    maxLevels = 1;
    while (nodes.size())
    {
      delete nodes.back();
      nodes.pop_back();
    }
  }

  void MarkAsActive(int id)
  {
    node::SceneNode* node = FindNode(id, NULL);
    node::PointLight* pointLight = dynamic_cast<node::PointLight*>(node);
    if (pointLight)
      activeLights.push_back(pointLight);
    else
      activeNodes.push_back(node);
  }
};