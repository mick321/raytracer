// APG_3D - program pro kresleni 3D grafiky bez pouziti knihoven DirectX nebo OpenGL
//        - pouziva se pouze WinAPI pro vytvoreni okna a kresleni pixelu (resp. bitmap)
#include "stdafx.h"
#include "raytracer.h"

#include "simpledrawing.h"
#include "ThreadWork.h"
#include "Scene.h"
#include "SceneLoader.h"

/* Create nice looking controls */

#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' \
					   version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' \
						language='*'\"")

enum 
{
  ID_FILE_LOADSCENE,
  ID_FILE_SAVEIMAGE,
	ID_FILE_EXIT,
  ID_RENDER,
  ID_STOP,
  ID_STATS,
  ID_ABOUTBOX
};

// Add new popup menu
#define ADDPOPUPMENU(hmenu, string) \
	HMENU hSubMenu = CreatePopupMenu(); \
	AppendMenu(hmenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, string);

// Add a menu item
#define ADDMENUITEM(hmenu, ID, string) \
	AppendMenu(hmenu, MF_STRING, ID, string);

// --------------------------------------------------------------------------------
#define MAX_LOADSTRING 100

// Globalni promenne
HINSTANCE hInst;								// Instance programu

HWND hWnd;                                      // Okno programu
HDC  hDC;

TCHAR szTitle[MAX_LOADSTRING];					// Nadpis okna
TCHAR szWindowClass[MAX_LOADSTRING];			// Jmeno tridy okna (window class)

// Prototypy funkci
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

SimpleDrawing*   simpleDraw;
ThreadWork*      threadwork;
Scene*           scene;

const TCHAR* autoSaveToFile = NULL;
bool finishedRendering = false;

#define TIMER_FOR_DRAW        1
#define TIMER_FOR_DRAW_PERIOD 500
 
// --------------------------------------------------------------------------------
void DrawBuffers()
{  
  simpleDraw->RenderBuffer(hDC);
}

// --------------------------------------------------------------------------------
void LoadScene()
{
  if (threadwork->IsWorking())
  {
    MessageBox(hWnd, "Cannot load scene: Raytracer is still working.", "Information", MB_OK | MB_ICONINFORMATION);
    return;
  }

  OPENFILENAME ofn;
  char szFileName[MAX_PATH] = "";
  char szCurrentDir[MAX_PATH + 16];
  GetCurrentDirectory(MAX_PATH, szCurrentDir);
  strcat(szCurrentDir, "\\scenes");
  szCurrentDir[MAX_PATH - 1] = '\0';

  ZeroMemory(&ofn, sizeof(ofn));
  ofn.lStructSize = sizeof(ofn);
  ofn.hwndOwner = hWnd;
  ofn.lpstrFilter = "Scene description (*.*)\0*.*\0";
  ofn.lpstrFile = szFileName;
  ofn.lpstrInitialDir = szCurrentDir;
  ofn.nMaxFile = MAX_PATH;
  ofn.Flags = OFN_EXPLORER | OFN_HIDEREADONLY;
  ofn.lpstrDefExt = NULL;
  if (GetOpenFileName(&ofn))
  {
    scene->ClearAll();

    SceneLoader loader(scene, threadwork);
    if (!loader.Load(szFileName))
    {
      MessageBox(hWnd, loader.err, "Error (load scene)", MB_OK | MB_ICONERROR);
    }
  }
}

// --------------------------------------------------------------------------------
void SaveImage()
{
  if (threadwork->IsWorking())
  {
    MessageBox(hWnd, "Cannot save image: Raytracer is still working.", "Information", MB_OK | MB_ICONINFORMATION);
    return;
  }

  if (!threadwork->ImageWasRendered())
  {
    MessageBox(hWnd, "Cannot save image: No scene has been rendered yet.", "Information", MB_OK | MB_ICONINFORMATION);
    return;
  }

  OPENFILENAME ofn;
  char szFileName[MAX_PATH] = "";

  ZeroMemory(&ofn, sizeof(ofn));

  ofn.lStructSize = sizeof(ofn);
  ofn.hwndOwner = hWnd;
  ofn.lpstrFilter = "PNG Image (*.png)\0*.png\0";
  ofn.lpstrFile = szFileName;
  ofn.nMaxFile = MAX_PATH;
  ofn.Flags = OFN_EXPLORER | OFN_HIDEREADONLY;
  ofn.lpstrDefExt = "txt";
  if (GetSaveFileName(&ofn))
  {
    char errormessage[128];
    if (!simpleDraw->SaveToPNG(szFileName, errormessage))
    {
      MessageBox(hWnd, errormessage, "Error (save to PNG)", MB_OK | MB_ICONERROR);
    }
  }
}

// --------------------------------------------------------------------------------
void StopRaytracer()
{
  scene->stop = true;
}

// --------------------------------------------------------------------------------
void ImageRendered()
{
  finishedRendering = true;
}

// --------------------------------------------------------------------------------
void CreateMainMenu(HWND hWnd)
{
  HMENU hMenu = CreateMenu();

	ADDPOPUPMENU(hMenu, "&File");	
  ADDMENUITEM(hSubMenu, ID_FILE_LOADSCENE, "&Load scene...");
	ADDMENUITEM(hSubMenu, ID_FILE_SAVEIMAGE, "&Save image...");
  ADDMENUITEM(hSubMenu, ID_FILE_EXIT, "&Exit");

  ADDMENUITEM(hMenu, ID_RENDER, "&Render");
  //ADDMENUITEM(hMenu, ID_STOP, "Stop");
  ADDMENUITEM(hMenu, ID_STATS, "S&tatistics");
  ADDMENUITEM(hMenu, ID_ABOUTBOX, "&About");

  SetMenu(hWnd, hMenu);
}

// --------------------------------------------------------------------------------
// Zaregistrovani tridy okna
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APG_3D));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= 0;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

// Inicializace instance okna
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
  hInst = hInstance;
  RECT rc;
  rc.left = 0;
  rc.top = 0;
  rc.right = 800;
  rc.bottom = 600;
  AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW & (~(WS_THICKFRAME | WS_MAXIMIZEBOX)), true);
  hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW & (~(WS_THICKFRAME | WS_MAXIMIZEBOX)),
    20, 20, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance, NULL);   

  if (!hWnd)
  {
    return FALSE;
  }

  simpleDraw = new SimpleDrawing(hWnd, GetWindowDC(hWnd));
  simpleDraw->ClearColor(0.8, 0.8, 0.8);

  scene = new Scene();
  threadwork = new ThreadWork(scene, simpleDraw);

  ShowWindow(hWnd, nCmdShow);
  UpdateWindow(hWnd);

  SetTimer(hWnd, TIMER_FOR_DRAW, TIMER_FOR_DRAW_PERIOD, NULL);
  return TRUE;
}

// -------------------------------------------------------------------------------
// Zpracovani zprav okna
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{	
  PAINTSTRUCT ps;
  //HDC hdc;

  switch (message)
  {	
  case WM_PAINT:      
    hDC = BeginPaint(hWnd, &ps);

    DrawBuffers();            

    EndPaint(hWnd, &ps);  

    if (finishedRendering)
    {
      if (autoSaveToFile)
      {
        char errormessage[128];
        if (simpleDraw->SaveToPNG(autoSaveToFile, errormessage))
          PostQuitMessage(0);
        else
          PostQuitMessage(-2);
      }
      finishedRendering = false;
    }

    break;    
  case WM_TIMER:
    if (wParam == TIMER_FOR_DRAW)
    {
      char newTitle[1024];
      char info[512];
      strcpy(newTitle, szTitle);
      threadwork->GetProcessInfo(info);
      if (strlen(info))
      {
        strcat(newTitle, " ");
        strcat(newTitle, info);
      }
      SetWindowText(hWnd, newTitle);
      InvalidateRect(hWnd, NULL, false);
    }
  case WM_KEYDOWN:
    break;
  case WM_LBUTTONDOWN:
    break;
  case WM_LBUTTONUP:
    break;
  case WM_RBUTTONDOWN:
    break;
  case WM_RBUTTONUP:
    break;
  case WM_SIZE:
    simpleDraw->OnWindowResize();       
    return DefWindowProc(hWnd, message, wParam, lParam);
  case WM_DESTROY:
    KillTimer(hWnd, TIMER_FOR_DRAW);
    PostQuitMessage(0);
    break;
  case WM_ERASEBKGND:
    return 0;
  case WM_COMMAND:
    switch (wParam)
    {
    case ID_RENDER:
      threadwork->Perform();
      break;
    case ID_STOP:
      StopRaytracer();
      break;
    case ID_FILE_LOADSCENE:
      LoadScene();
      break;
    case ID_FILE_SAVEIMAGE:
      SaveImage();
      break;
    case ID_FILE_EXIT:
      PostQuitMessage(0);
      break;
    case ID_ABOUTBOX:
      MessageBox(hWnd, "This application has been developed as semestral project\n on University of West Bohemia by Michal Zak.\n\n2013", "About", MB_OK | MB_ICONINFORMATION);
      break;
    case ID_STATS:
      {
        char szFullInfo[1024];
        threadwork->GetFullProcessInfo(szFullInfo);
        MessageBox(hWnd, szFullInfo, "Statistics", MB_OK | MB_ICONINFORMATION);
      }
      break;
    }
  default:
    return DefWindowProc(hWnd, message, wParam, lParam);
  }
  return 0;
}

void ProcessCommandLine()
{
  if (__argc >= 2) // autoload
  {
    scene->ClearAll();
    SceneLoader loader(scene, threadwork);
    if (!loader.Load(__argv[1]))
      exit(-1);

    if (__argc >= 3) // autosave
    {
      autoSaveToFile = __argv[2];
    }
  }
}

// --------------------------------------------------------------------------------
// Vstupni funkce programu.
int APIENTRY _tWinMain(HINSTANCE hInstance,
  HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

  srand(GetTickCount()); // random seed
     
	MSG msg;
	HACCEL hAccelTable;
     
	strcpy_s(szTitle, "Raytracer [Michal Zak, APG 13/14]");
	LoadString(hInstance, IDC_APG_3D, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);
     
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}  

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_APG_3D));
  CreateMainMenu(hWnd);

  ProcessCommandLine();

	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

  delete threadwork;
  delete simpleDraw;
  delete scene;
	return (int) msg.wParam;
}