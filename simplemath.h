#pragma once

#define FLOAT_TYPE double

#ifndef PI180
#define PI180	(FLOAT_TYPE)0.0174532925199432957692369076848861f
#endif

namespace sm
{
  inline FLOAT_TYPE random(FLOAT_TYPE rangeFrom, FLOAT_TYPE rangeTo)
  {
    return rangeFrom + static_cast <FLOAT_TYPE> (rand()) / (static_cast <FLOAT_TYPE> (RAND_MAX/(rangeTo-rangeFrom)));
  }

  inline FLOAT_TYPE randomGauss(FLOAT_TYPE m, FLOAT_TYPE d)
  {
    FLOAT_TYPE rtn = 0;
    static const int iterations = 40;
    for (int i = 0; i < iterations; i++)
      rtn += random(-1, 1);

    const FLOAT_TYPE variance = (FLOAT_TYPE)2 * (FLOAT_TYPE)iterations / (FLOAT_TYPE)12;

    rtn *= d/variance;
    rtn += m;
    return rtn;
  }

  struct vec3
  {
    FLOAT_TYPE x, y, z;
    /// constructors
    inline vec3() { x = 0; y = 0; z = 0; }
    inline vec3(const FLOAT_TYPE val)
    {
      this->x = val;
      this->y = val;
      this->z = val;
    }
    inline vec3(const FLOAT_TYPE x, const FLOAT_TYPE y, const FLOAT_TYPE z) 
    { 
      this->x = x;
      this->y = y;
      this->z = z;
    }
    /// set components
    inline void set(const FLOAT_TYPE x, const FLOAT_TYPE y, const FLOAT_TYPE z)
    {
      this->x = x;
      this->y = y;
      this->z = z;
    }
  };

  struct vec4
  {
    FLOAT_TYPE x, y, z, w;
    /// constructors

    inline vec4() { x = 0; y = 0; z = 0; w = 1; }
    
    inline vec4(const FLOAT_TYPE val)
    {
      this->x = val;
      this->y = val;
      this->z = val;
      this->w = val;
    }

    inline vec4(const FLOAT_TYPE x, const FLOAT_TYPE y, const FLOAT_TYPE z, const FLOAT_TYPE w) 
    { 
      this->x = x;
      this->y = y;
      this->z = z;
      this->w = w;
    }

    inline vec4(const vec3& v, const FLOAT_TYPE w) 
    { 
      this->x = v.x;
      this->y = v.y;
      this->z = v.z;
      this->w = w;
    }

    inline vec3 xyz() const
    {
      vec3 rtn;
      rtn.x = x;
      rtn.y = y;
      rtn.z = z;
      return rtn;
    }

    /// set components
    inline void set(const FLOAT_TYPE x, const FLOAT_TYPE y, const FLOAT_TYPE z, const FLOAT_TYPE w)
    {
      this->x = x;
      this->y = y;
      this->z = z;
      this->w = w;
    }
  };

  inline vec3 operator* (const vec3& lhs, const vec3& rhs)
  {
    return vec3(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z);
  }

  inline vec3 operator* (const vec3& lhs, const FLOAT_TYPE rhs)
  {
    return vec3(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
  }

  inline vec3 operator* (const FLOAT_TYPE lhs, const vec3& rhs)
  {
    return vec3(rhs.x * lhs, rhs.y * lhs, rhs.z * lhs);
  }

  inline vec3 operator+ (const vec3& lhs, const vec3& rhs)
  {
    return vec3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
  }

  inline vec3 operator- (const vec3& lhs, const vec3& rhs)
  {
    return vec3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
  }

  inline vec3 operator- (const vec3& rhs)
  {
    return vec3(-rhs.x, -rhs.y, -rhs.z);
  }

  inline vec3 lerp(const vec3& a, const vec3& b, const FLOAT_TYPE t)
  {
    return (1 - t) * a + t * b;
  };

  inline const FLOAT_TYPE dot(const vec3& a, const vec3& b)
  {
    return a.x * b.x + a.y * b.y + a.z * b.z;
  };

  inline vec3 cross(const vec3& a, const vec3& b)
  {
    return vec3(a.y*b.z - a.z*b.y,
					      a.z*b.x - a.x*b.z,
					      a.x*b.y - a.y*b.x);
  };

  inline vec3 reflect(const vec3& v, const vec3& n)
  {
    return v - 2.0 * dot(v, n) * n;
  }

  inline vec3 refract(const vec3& v, const vec3& n, const FLOAT_TYPE eta)
  {
    const FLOAT_TYPE NdotV = dot(n, v);
    const FLOAT_TYPE k = 1.0 - eta * eta * (1.0 - NdotV * NdotV);
    if (k < 0.0)
        return vec3(0.0);
    else
        return eta * v - (eta * NdotV + sqrt(k)) * n;
  }

  inline FLOAT_TYPE length2(const vec3& v)
  {
    return v.x * v.x + v.y * v.y + v.z * v.z;
  }

  inline FLOAT_TYPE length(const vec3& v)
  {
    return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
  }

  inline vec3 normalize(const vec3& v)
  {
    FLOAT_TYPE invlen = 1.0 / length(v);
    return v * invlen;
  }

  inline vec4 operator* (const vec4& lhs, const FLOAT_TYPE rhs)
  {
    return vec4(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs);
  }

  inline vec4 operator* (const FLOAT_TYPE lhs, const vec4& rhs)
  {
    return vec4(rhs.x * lhs, rhs.y * lhs, rhs.z * lhs, rhs.w * lhs);
  }

  inline vec4 operator* (const vec4& lhs, const vec4& rhs)
  {
    return vec4(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w);
  }

  inline vec4 operator+ (const vec4& lhs, const vec4& rhs)
  {
    return vec4(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w);
  }

  inline vec4 operator- (const vec4& lhs, const vec4& rhs)
  {
    return vec4(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w);
  }

  inline vec4 operator- (const vec4& rhs)
  {
    return vec4(-rhs.x, -rhs.y, -rhs.z, -rhs.w);
  }

  inline vec4 lerp(const vec4& a, const vec4& b, const FLOAT_TYPE t)
  {
    return (1 - t) * a + t * b;
  };

  inline const FLOAT_TYPE dot(const vec4& a, const vec4& b)
  {
    return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
  };

  inline const FLOAT_TYPE saturate(const FLOAT_TYPE value)
  {
    return min(1, max(0, value));
  }

  inline vec3 phongNormalized(const vec3& normal, const vec3& toViewer, const vec3& toLight, const vec3& ambient, const vec3& diffuse, const vec3& specular,
    const FLOAT_TYPE shininess)
  {
    FLOAT_TYPE iDiffuse = saturate(dot(normal, toLight));
    const vec3 reflected = reflect(-toViewer, normal);
    FLOAT_TYPE iSpecular = (iDiffuse > 0) ? pow(saturate(dot(reflected, toLight)), shininess) : 0;
    return ambient + diffuse * iDiffuse + specular * iSpecular;
  }
  
  inline vec3 phong(const vec3& normal, const vec3& toViewer, const vec3& toLight, const vec3& ambient, const vec3& diffuse, const vec3& specular,
    const FLOAT_TYPE shininess)
  {
    return phongNormalized(normalize(normal), normalize(toViewer), normalize(toLight), ambient, diffuse, specular, shininess);
  }

  inline bool solveQuadraticEquation(const FLOAT_TYPE a, const FLOAT_TYPE b, const FLOAT_TYPE c, FLOAT_TYPE& x1, FLOAT_TYPE& x2)
  {
    const FLOAT_TYPE det = b*b - 4*a*c;
    if (det < 0)
      return false;
    const FLOAT_TYPE sqrtDet = sqrt(det);
    const FLOAT_TYPE div2a = 1.0/(2*a);
    x1 = (-b - sqrtDet) * div2a;
    x2 = (-b + sqrtDet) * div2a;
    return true;
  }

  /// Ray
  struct ray
  {
    vec3 pos;
    vec3 dir;
  };

  /// Matrix 4x4
  struct mat4
  {
    union
    {
      struct
      {
        FLOAT_TYPE m[4][4];
      };
      struct
      {
        FLOAT_TYPE m11, m12, m13, m14;
        FLOAT_TYPE m21, m22, m23, m24;
        FLOAT_TYPE m31, m32, m33, m34;
        FLOAT_TYPE m41, m42, m43, m44;
      };
    };

    inline mat4()
    {
      m11 = 1; m12 = 0; m13 = 0; m14 = 0;
      m21 = 0; m22 = 1; m23 = 0; m24 = 0;
      m31 = 0; m32 = 0; m33 = 1; m34 = 0;
      m41 = 0; m42 = 0; m43 = 0; m44 = 1;
    }

    inline mat4::mat4( const FLOAT_TYPE Am11, const FLOAT_TYPE Am12, const FLOAT_TYPE Am13, const FLOAT_TYPE Am14,
						   const FLOAT_TYPE Am21, const FLOAT_TYPE Am22, const FLOAT_TYPE Am23, const FLOAT_TYPE Am24,
						   const FLOAT_TYPE Am31, const FLOAT_TYPE Am32, const FLOAT_TYPE Am33, const FLOAT_TYPE Am34,
						   const FLOAT_TYPE Am41, const FLOAT_TYPE Am42, const FLOAT_TYPE Am43, const FLOAT_TYPE Am44)
    {
	    m11 = Am11; m12 = Am12; m13 = Am13; m14 = Am14;
	    m21 = Am21; m22 = Am22; m23 = Am23; m24 = Am24;
	    m31 = Am31; m32 = Am32; m33 = Am33; m34 = Am34;
	    m41 = Am41; m42 = Am42; m43 = Am43; m44 = Am44;
    }

    inline mat4 operator * (const FLOAT_TYPE& rhs) const
    {
      mat4 rtn;
      rtn.m11 = m11 * rhs; rtn.m12 = m12 * rhs; rtn.m13 = m13 * rhs; rtn.m14 = m14 * rhs;
      rtn.m21 = m21 * rhs; rtn.m22 = m22 * rhs; rtn.m23 = m23 * rhs; rtn.m24 = m24 * rhs;
      rtn.m31 = m31 * rhs; rtn.m32 = m32 * rhs; rtn.m33 = m33 * rhs; rtn.m34 = m34 * rhs;
      rtn.m41 = m41 * rhs; rtn.m42 = m42 * rhs; rtn.m43 = m43 * rhs; rtn.m44 = m44 * rhs;
      return rtn;
    }

    inline vec4 operator * (const vec4& rhs) const
    {
      vec4 rtn(0, 0, 0, 0);
      rtn.x = m11 * rhs.x + m12 * rhs.y + m13 * rhs.z + m14 * rhs.w;
      rtn.y = m21 * rhs.x + m22 * rhs.y + m23 * rhs.z + m24 * rhs.w;
      rtn.z = m31 * rhs.x + m32 * rhs.y + m33 * rhs.z + m34 * rhs.w;
      rtn.w = m41 * rhs.x + m42 * rhs.y + m43 * rhs.z + m44 * rhs.w;
      return rtn;
    };

    inline mat4 mat4::inverse() const
    {
      mat4 A;
      const FLOAT_TYPE M3344 = m33 * m44 - m34 * m43;
      const FLOAT_TYPE M3244 = m32 * m44 - m34 * m42;
      const FLOAT_TYPE M3243 = m32 * m43 - m33 * m42;
      const FLOAT_TYPE M2344 = m23 * m44 - m24 * m43;
      const FLOAT_TYPE M2244 = m22 * m44 - m24 * m42;
      const FLOAT_TYPE M2243 = m22 * m43 - m23 * m42;
      const FLOAT_TYPE M2334 = m23 * m34 - m24 * m33;
      const FLOAT_TYPE M2234 = m22 * m34 - m24 * m32;
      const FLOAT_TYPE M2233 = m22 * m33 - m23 * m32;
      const FLOAT_TYPE M3144 = m31 * m44 - m34 * m41;
      const FLOAT_TYPE M3143 = m31 * m43 - m33 * m41;
      const FLOAT_TYPE M2144 = m21 * m44 - m24 * m41;
      const FLOAT_TYPE M2143 = m21 * m43 - m23 * m41;
      const FLOAT_TYPE M2134 = m21 * m34 - m24 * m31;
      const FLOAT_TYPE M2133 = m21 * m33 - m23 * m31;
      const FLOAT_TYPE M3142 = m31 * m42 - m32 * m41;
      const FLOAT_TYPE M2142 = m21 * m42 - m22 * m41;
      const FLOAT_TYPE M2132 = m21 * m32 - m22 * m31;

      A.m11 =  m22 * M3344 - m23 * M3244 + m24 * M3243;
      A.m12 = -m12 * M3344 + m13 * M3244 - m14 * M3243;
      A.m13 =  m12 * M2344 - m13 * M2244 + m14 * M2243;
      A.m14 = -m12 * M2334 + m13 * M2234 - m14 * M2233;

      A.m21 = -m21 * M3344 + m23 * M3144 - m24 * M3143;
      A.m22 =  m11 * M3344 - m13 * M3144 + m14 * M3143;
      A.m23 = -m11 * M2344 + m13 * M2144 - m14 * M2143;
      A.m24 =  m11 * M2334 - m13 * M2134 + m14 * M2133;

      A.m31 =  m21 * M3244 - m22 * M3144 + m24 * M3142;
      A.m32 = -m11 * M3244 + m12 * M3144 - m14 * M3142;
      A.m33 =  m11 * M2244 - m12 * M2144 + m14 * M2142;
      A.m34 = -m11 * M2234 + m12 * M2134 - m14 * M2132;

      A.m41 = -m21 * M3243 + m22 * M3143 - m23 * M3142;
      A.m42 =  m11 * M3243 - m12 * M3143 + m13 * M3142;
      A.m43 = -m11 * M2243 + m12 * M2143 - m13 * M2142;
      A.m44 =  m11 * M2233 - m12 * M2133 + m13 * M2132;

      A = A * ((FLOAT_TYPE)1 / (m11 * A.m11 + m12 * A.m21 + m13 * A.m31 + m14 * A.m41));   // Determinant
      return A;
    }

    inline mat4 mat4::transpose() const
    {
      return mat4(m11, m21, m31, m41,
        m12, m22, m32, m42,
        m13, m23, m33, m43,
        m14, m24, m34, m44 );
    }

    static inline mat4 translation(const FLOAT_TYPE x, const FLOAT_TYPE y, const FLOAT_TYPE z)
    {
      mat4 rtn;
      rtn.m14 = x;
      rtn.m24 = y;
      rtn.m34 = z;
      return rtn;
    }

    static inline mat4 translation(const vec3 v)
    {
      return translation(v.x, v.y, v.z);
    }

    static inline mat4 scale(const FLOAT_TYPE sx, const FLOAT_TYPE sy, const FLOAT_TYPE sz)
    {
      mat4 rtn;
      rtn.m11 = sx;
      rtn.m22 = sy;
      rtn.m33 = sz;
      return rtn;
    }

    static inline mat4 rotationX(const FLOAT_TYPE angle)
    {
      mat4 rtn;
      const float c = (float) cos( PI180*angle );
	    const float s = (float) sin( PI180*angle );
      rtn.m22 = c; rtn.m23 = -s;
      rtn.m32 = s; rtn.m33 = c;
      return rtn;
    }

    static inline mat4 rotationY(const FLOAT_TYPE angle)
    {
      mat4 rtn;
      const float c = (float) cos( PI180*angle );
	    const float s = (float) sin( PI180*angle );
      rtn.m11 = c; rtn.m13 = s;
      rtn.m31 = -s; rtn.m33 = c;
      return rtn;
    }

    static inline mat4 rotationZ(const FLOAT_TYPE angle)
    {
      mat4 rtn;
      const float c = (float) cos( PI180*angle );
	    const float s = (float) sin( PI180*angle );
      rtn.m11 = c; rtn.m12 = -s;
      rtn.m21 = s; rtn.m11 = c;
      return rtn;
    }

  };

  inline mat4 operator* (const mat4& a, const mat4& b)
  {
	  mat4 c;
	
	  c.m11 = a.m11*b.m11 + a.m12*b.m21 + a.m13*b.m31 + a.m14*b.m41;
	  c.m12 = a.m11*b.m12 + a.m12*b.m22 + a.m13*b.m32 + a.m14*b.m42;
	  c.m13 = a.m11*b.m13 + a.m12*b.m23 + a.m13*b.m33 + a.m14*b.m43;
	  c.m14 = a.m11*b.m14 + a.m12*b.m24 + a.m13*b.m34 + a.m14*b.m44;

	  c.m21 = a.m21*b.m11 + a.m22*b.m21 + a.m23*b.m31 + a.m24*b.m41;
	  c.m22 = a.m21*b.m12 + a.m22*b.m22 + a.m23*b.m32 + a.m24*b.m42;
	  c.m23 = a.m21*b.m13 + a.m22*b.m23 + a.m23*b.m33 + a.m24*b.m43;
	  c.m24 = a.m21*b.m14 + a.m22*b.m24 + a.m23*b.m34 + a.m24*b.m44;

	  c.m31 = a.m31*b.m11 + a.m32*b.m21 + a.m33*b.m31 + a.m34*b.m41;
	  c.m32 = a.m31*b.m12 + a.m32*b.m22 + a.m33*b.m32 + a.m34*b.m42;
	  c.m33 = a.m31*b.m13 + a.m32*b.m23 + a.m33*b.m33 + a.m34*b.m43;
	  c.m34 = a.m31*b.m14 + a.m32*b.m24 + a.m33*b.m34 + a.m34*b.m44;

	  c.m41 = a.m41*b.m11 + a.m42*b.m21 + a.m43*b.m31 + a.m44*b.m41;
	  c.m42 = a.m41*b.m12 + a.m42*b.m22 + a.m43*b.m32 + a.m44*b.m42;
	  c.m43 = a.m41*b.m13 + a.m42*b.m23 + a.m43*b.m33 + a.m44*b.m43;
	  c.m44 = a.m41*b.m14 + a.m42*b.m24 + a.m43*b.m34 + a.m44*b.m44;

    return c;
  };

  // quadric utilities
  inline vec4 quadricGradient(const mat4& Q, const vec4& pos)
  {
    vec4 rtn;
    rtn.x = 2 * pos.x * Q.m11 + Q.m12 * pos.y + Q.m13 * pos.z + Q.m14 * pos.w 
      + pos.y * Q.m21 + pos.z * Q.m31 + pos.w * Q.m41;
    rtn.y = 2 * pos.y * Q.m22 + Q.m21 * pos.x + Q.m23 * pos.z + Q.m24 * pos.w 
      + pos.x * Q.m12 + pos.z * Q.m32 + pos.w * Q.m42;
    rtn.z = 2 * pos.z * Q.m33 + Q.m31 * pos.x + Q.m32 * pos.y + Q.m34 * pos.w 
      + pos.x * Q.m13 + pos.y * Q.m23 + pos.w * Q.m43;
    rtn.w = 2 * pos.w * Q.m44 + Q.m41 * pos.x + Q.m42 * pos.y + Q.m43 * pos.z
      + pos.x * Q.m14 + pos.y * Q.m24 + pos.z * Q.m34;
    return rtn;
  }

  inline vec4 computePlaneCoef(const vec3& A, const vec3& B, const vec3& C)
  {
    vec3 n = normalize(cross(B - A, C - A));
    return vec4(n, -dot(n, A));
  }
}